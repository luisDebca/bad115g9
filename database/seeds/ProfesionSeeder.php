<?php

use Illuminate\Database\Seeder;

class ProfesionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('Profesiones')->insert([
            'nombre' => 'Tecnico Informatico',
            'descripcion' => 'Encargado del equipo de TI',
        ]);

        \DB::table('Profesiones')->insert([
            'nombre' => 'Ingeniero de Software',
            'descripcion' => 'Jefe de Proyectos, de la empresa',
        ]);
    }
}
