<?php

use Illuminate\Database\Seeder;

class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Paises')->insert([
            'nombre' => 'El Salvador',
        ]);

        \DB::table('Paises')->insert([
            'nombre' => 'Estados Unidos',
        ]);

        DB::table('Paises')->insert([
            'nombre' => 'Colombia',
        ]);
    }
}
