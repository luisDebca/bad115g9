<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         $this->call(RolesTableSeeder::class);
         $this->call(EmpresasTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(PaisSeeder::class);
         $this->call(RegionSeeder::class);
         $this->call(SubregionSeeder::class);
         $this->call(EstadoCivilSeeder::class);
         $this->call(ProfesionSeeder::class);
         $this->call(GeneroSeeder::class);
    }
}
