<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB:: table('Roles')->insert(array(
            'nombre' =>'Administrador del Sistema',
            'descripcion'=>'Gestiona todo lo referente a Tecnologias de la informacion',
        	));

        \DB:: table('Roles')->insert(array(
            'nombre' =>'Gerente General',
            'descripcion'=>'Gestiona todo lo referente a la Empresa',
        	));

        \DB:: table('Roles')->insert(array(
            'nombre' =>'Gerente de Recursos Humanos',
            'descripcion'=>'Gestiona todo lo referente a recursos humanos',
        	));

        \DB:: table('Roles')->insert(array(
            'nombre' =>'Jefe de Unidad Organizacional',
            'descripcion'=>'Gestiona todo lo referente a las unidades de la institucion',
        	));

    }
}
