<?php

use Illuminate\Database\Seeder;

class EstadoCivilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('EstadoCiviles')->insert([
            'tipo' => 'Casado/a',
        ]);

        \DB::table('EstadoCiviles')->insert([
            'tipo' => 'Soltero/a',
        ]);

  
    }
}
