<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionContratoTrabajoUnidadOrganizacional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ContratosTrabajo', function (Blueprint $table) {
            $table->unsignedInteger('unidad_id');
            $table->foreign('unidad_id')
                ->references('id')
                ->on('UnidadesOrganizacionales')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ContratosTrabajo', function (Blueprint $table) {
            $table->dropColumn('unidad_id');
        });
    }
}
