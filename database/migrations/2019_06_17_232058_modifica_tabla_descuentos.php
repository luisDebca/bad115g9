<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificaTablaDescuentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Descuentos', function (Blueprint $table) {
            $table->double('montoFijo',8,2)->nullable(true);
            $table->enum('tipo',['LEY','FIJO','BASE','RENTA'])->default('LEY');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Descuentos', function (Blueprint $table) {
            $table->dropColumn('montoFijo');
            $table->dropColumn('tipo');
        });
    }
}
