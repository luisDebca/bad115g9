<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaCentroCostos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CentroCostos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero',8);
            $table->double('monto',8,2);
            $table->string('periocidad',10);
            $table->date('anioCalendario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CentroCostos');
    }
}
