<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionEmpleadoGenero extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Empleados', function (Blueprint $table) {
            $table->unsignedInteger('genero_id')->nullable();
            $table->foreign('genero_id')
                ->references('id')
                ->on('Generos')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Empleados', function (Blueprint $table) {
            $table->dropColumn('genero_id');
        });
    }
}
