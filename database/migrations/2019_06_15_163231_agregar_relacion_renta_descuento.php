<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionRentaDescuento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Descuentos', function (Blueprint $table) {
            $table->unsignedInteger('renta_id')->nullable(true);

            $table->foreign('renta_id')
                ->references('id')
                ->on('Rentas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Descuentos', function (Blueprint $table) {
           $table->dropColumn('renta_id');
        });
    }
}
