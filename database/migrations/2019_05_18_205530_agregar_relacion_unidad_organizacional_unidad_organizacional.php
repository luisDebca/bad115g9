<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionUnidadOrganizacionalUnidadOrganizacional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('UnidadesOrganizacionales', function (Blueprint $table) {
            $table->unsignedInteger('unidadPadre_id')->nullable();
            $table->foreign('unidadPadre_id')
                ->references('id')
                ->on('UnidadesOrganizacionales')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('UnidadesOrganizacionales', function (Blueprint $table) {
            $table->dropColumn('unidadPadre_id');
        });
    }
}
