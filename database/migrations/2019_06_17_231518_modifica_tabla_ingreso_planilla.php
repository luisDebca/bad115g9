<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificaTablaIngresoPlanilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('IngresosPlanilla', function (Blueprint $table) {
            $table->date('desde')->nullable(true);
            $table->date('hasta')->nullable(true);
            $table->boolean('estado')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('IngresosPlanilla', function (Blueprint $table) {
            $table->dropColumn('desde');
            $table->dropColumn('hasta');
            $table->dropColumn('estado');
        });
    }
}
