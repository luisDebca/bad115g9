<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionUnidadOrganizacionalEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('UnidadesOrganizacionales', function (Blueprint $table) {
            $table->unsignedInteger('empresa_id');
            $table->foreign('empresa_id')
                ->references('id')
                ->on('Empresas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('UnidadesOrganizacionales', function (Blueprint $table) {
            $table->dropColumn('empresa_id');
        });
    }
}
