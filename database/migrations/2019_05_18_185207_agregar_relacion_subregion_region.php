<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionSubregionRegion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Subregiones', function (Blueprint $table) {
            $table->unsignedInteger('region_id');

                        $table->foreign('region_id')
                            ->references('id')
                            ->on('Regiones')
                            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Subregiones', function (Blueprint $table) {
           $table->dropColumn('region_id');
        });
    }
}
