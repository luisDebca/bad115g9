<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionJefeSubalterno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Empleados', function (Blueprint $table) {
            $table->unsignedInteger('jefe_id')->nullable();
            $table->foreign('jefe_id')
                ->references('id')
                ->on('Empleados')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Empleados', function (Blueprint $table) {
            $table->dropColumn('jefe_id');
        });
    }
}
