<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaUnidadesOrganizacionales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UnidadesOrganizacionales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo',12)->nullable();
            $table->string('nombre',100);
            $table->enum('tipo', ['Departamento', 'Area', 'Seccion','Sub-Seccion','Unidad'])->default('Departamento');
            $table->string('descripcion',500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UnidadesOrganizacionales');
    }
}
