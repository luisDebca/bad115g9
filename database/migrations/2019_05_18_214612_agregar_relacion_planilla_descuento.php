<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionPlanillaDescuento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DescuentosPlanilla', function (Blueprint $table) {
            $table->unsignedInteger('planilla_id');
            $table->unsignedInteger('descuento_id');
            $table->double('monto',8,2);
            $table->foreign('planilla_id')
                ->references('id')
                ->on('Planillas')
                ->onDelete('cascade');
            $table->foreign('descuento_id')
                ->references('id')
                ->on('Descuentos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('DescuentosPlanilla');
    }
}
