<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionContratoTrabajoEmpleado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ContratosTrabajo', function (Blueprint $table) {
            $table->unsignedInteger('empleado_id');
            $table->foreign('empleado_id')
                ->references('id')
                ->on('Empleados')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ContratosTrabajo', function (Blueprint $table) {
            $table->dropColumn('empleado_id');
        });
    }
}
