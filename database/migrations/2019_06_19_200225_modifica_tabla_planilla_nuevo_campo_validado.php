<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificaTablaPlanillaNuevoCampoValidado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Planillas', function (Blueprint $table) {
            $table->boolean('validado')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Planillas', function (Blueprint $table) {
            $table->dropColumn('validado');
        });
    }
}
