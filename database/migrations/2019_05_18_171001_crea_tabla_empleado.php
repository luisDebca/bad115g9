<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaEmpleado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo',15)->unique();
            $table->string('primerNombre',50);
            $table->string('segundoNombre',50);
            $table->string('apellidoPaterno',50);
            $table->string('apellidoMaterno',50);
            $table->string('apellidoCasada',50)->nullable();
            $table->date('fechaNacimiento');
            $table->string('dui',10)->unique();
            $table->string('nit',17)->unique()->nullable();
            $table->string('isss',10)->unique()->nullable();
            $table->string('nup',12)->unique()->nullable();
            $table->string('pasaporte',10)->unique()->nullable();
            $table->string('emailPersonal',150)->nullable();
            $table->string('emailInstitucional',150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Empleados');
    }
}
