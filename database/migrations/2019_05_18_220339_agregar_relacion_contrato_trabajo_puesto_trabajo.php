<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionContratoTrabajoPuestoTrabajo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ContratosTrabajo', function (Blueprint $table) {
            $table->unsignedInteger('puesto_id')->nullable();
            $table->foreign('puesto_id')
                ->references('id')
                ->on('PuestosTrabajo')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ContratosTrabajo', function (Blueprint $table) {
            $table->dropColumn('puesto_id');
        });
    }
}
