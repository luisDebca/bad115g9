<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaComision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Comisiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50)->unique();
            $table->double('valorMenor',8,2);
            $table->double('valorMayor',8,2);
            $table->double('taza',8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Comisiones');
    }
}
