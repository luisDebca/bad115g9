<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarRelacionPlanillaContratoTrabajo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Planillas', function (Blueprint $table) {
            $table->unsignedInteger('contrato_id')->nullable();
            $table->foreign('contrato_id')
                ->references('id')
                ->on('ContratosTrabajo')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Planillas', function (Blueprint $table) {
            $table->dropColumn('contrato_id');
        });
    }
}
