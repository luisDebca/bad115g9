<li>
    <a href="{{ url('puestos') }}">
        <i class="material-icons">card_travel</i>
        <span>Puestos de trabajo</span>
    </a>
</li>
<li>
    <a href="{{ route('descuentos.index') }}">
        <i class="material-icons">vertical_align_bottom</i>
        <span>Descuentos</span>
    </a>
</li>
<li>
    <a href="{{ route('ingresos.index') }}">
        <i class="material-icons">vertical_align_top</i>
        <span>Ingresos</span>
    </a>
</li>
<li>
    <a href="{{ route('comisiones.index') }}">
        <i class="material-icons">monetization_on</i>
        <span>Comisiones</span>
    </a>
</li>
<li>
    <a href="{{ route('planillas.index')}}">
        <i class="material-icons">bookmarks</i>
        <span>Planillas</span>
    </a>
</li>
<li>
    <a href="{{ route('rentas.index')}}">
        <i class="material-icons">account_balance</i>
        <span>Renta</span>
    </a>
</li>