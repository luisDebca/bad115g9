<li>
    <a href="{{ url('usuarios') }}">
        <i class="material-icons">people_outline</i>
        <span>Administrar Usuarios</span>
    </a>
</li>
<li>
    <a href="{{ route('ad-empresa.index') }}">
        <i class="material-icons">work</i>
        <span>Empresas</span>
    </a>
</li>