<li>
    <a href="{{ url('empresa') }}">
        <i class="material-icons">work</i>
        <span>Empresa</span>
    </a>
</li>
<li>
    <a href="{{ url('unidad-organizativa') }}">
        <i class="material-icons">sort</i>
        <span>Unidades Organizativas</span>
    </a>
</li>

<li>
    <a href="{{ url('centro-costos') }}">
        <i class="material-icons">attach_money</i>
        <span>Centro de Costo</span>
    </a>
</li>