<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info" style="background-color: #3d3d3d">
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}}</div>
            <div class="email">{{Auth::user()->email}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="javascript:void(0);"><i class="material-icons">person</i>Perfil</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ url('/logout') }}"><i class="material-icons">input</i>Salir</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MENU</li>
            <li class="active">
                <a href="{{url('/')}}">
                    <i class="material-icons">home</i>
                    <span>Inicio</span>
                </a>
            </li>
            @if(Auth::user()->rol_id == 1)
                @include('layouts.menu.admin')
            @elseif(Auth::user()->rol_id == 2)
                @include('layouts.menu.gerente')
            @elseif(Auth::user()->rol_id == 3)
                @include('layouts.menu.rrhh')
            @elseif(Auth::user()->rol_id == 4)
                @include('layouts.menu.uo')
            @endif
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div><i class="material-icons">access_time</i> <a id="digital-clock" href="javascript:void(0);" class="pull-right text-primary"></a></div>
        <div class="copyright">
            &copy; 2019 <a href="javascript:void(0);">Sistema de planillas - DAB115</a>.
        </div>
    </div>
    <!-- #Footer -->
</aside>