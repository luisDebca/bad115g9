@extends('layouts.appTable')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li class="active"><i class="material-icons">archive</i>Unidades Organizativas</li>
        </ol>
    </div>
    @if(session()->get('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Listado de Departamentos
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('unidad-organizativa.create')}}"><i class="material-icons">add</i>Departamento</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Fecha de creación</th>
                                <th>Ultima Fecha de actualización</th>
                                <th>Accion</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($unidades as $unidad)
                                <tr>
                                    <td>{{$unidad->codigo}}</td>
                                    <td>{{$unidad->nombre}}</td>
                                    <td>{{$unidad->descripcion}}</td>
                                    <td>{{$unidad->created_at->format('Y-M-d')}}</td>
                                    <td>{{$unidad->updated_at->format('Y-M-d')}}</td>
                                    <td>
                                        <ul class="list-unstyled">
                                            <li class="dropdown">
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>{{ method_field('get') }}<a href="{{ route('unidad-organizativa.show',$unidad->id) }}"><i class="material-icons">add</i>Subunidad</a></li>
                                                    <li><a href="{{ route('unidad-organizativa.edit', $unidad->id) }}"><i class="material-icons">edit</i> Editar</a></li>
                                                    <li><a href="javascript:" class="deleteBtn" data-name="{{ $unidad->nombre }}" data-id="{{ $unidad->id }}"><i class="material-icons">delete</i> Eliminar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="delete-form" method="post" action="javascript:">
                    {{ method_field('delete') }}
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Eliminar unidad de trabajo</h4>
                    </div>
                    <div class="modal-body">
                        Se eliminara la unidad de <strong id="to-delete-name"></strong>
                        <p>¿Desea continuar?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger waves-effect">Eliminar</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(".deleteBtn").on('click',function () {
            var id = $(this).data('id');
            var name = $(this).data('name');
            $("#to-delete-name").text(name);
            $("#delete-form").attr('action',window.location.href + '/' + id);
            $("#deleteModal").modal("show");
        });
    </script>
@endsection