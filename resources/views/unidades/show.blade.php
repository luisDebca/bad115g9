@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{url('/')}}"><i class="material-icons">list</i> Unidades Organizativas</a></li>
        <li class="active"><i class="material-icons">archive</i>Agregar Subunidad</li>
        </ol>
    </div>
    @if(session()->get('status'))
    <div class="alert alert-success">
        {{ session()->get('status') }}
    </div>
@endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Nuevo Area del {{$depto->tipo}} 
                    </h2>
                </div>
                <div class="body masked-input">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('unidad-organizativa.insert') }}">
                         {!! csrf_field() !!}
                     <table class="table table-striped">
                             <thead>
                                 <tr>
                                     <th>N°</th>
                                     <th></th>
                                     <th>Nombre</th>
                                     <th>Descripcion</th>
                                     <th>Delete</th>
                                 </tr>
                             </thead>
                             <tbody class="resultbody">
                                 <tr>
                                     <td class="no">1</td>                                     
                                     <td>
                                        <input type="hidden" class="unidadPadre_id form-control" name="unidadPadre_id[]" value="{{$id}}">
                                    </td>
                                     <td>
                                         <input type="text" class="nombre form-control" name="nombre[]" value="{{ old('nombre') }}">
                                     </td>
                                     <td>
                                         <input type="textarea" class="descripcion form-control" name="descripcion[]" value="{{ old('descripcion') }}">
                                     </td>
                                     <td>
                                         <input type="button" class="btn btn-danger delete" value="x">
                                     </td>
                                 </tr> 
                             </tbody>
                         </table>    
                         <center><input type="button" class="btn btn-lg btn-primary add" value="+ Registro">   
                         <input type="submit" class="btn btn-lg btn-default" value="Guardar"></center>
                         </form>
                         
                </div>
                

                @if($count ==0)
         
         @else
         
         <div class="body">
            <div class="table-responsive">
                <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                    <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Fecha de creación</th>
                        <th>Ultima Fecha de actualización</th>
                        <th>Accion</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($unidades as $unidad)
                        <tr>
                            <td>{{$unidad->codigo}}</td>
                            <td>{{$unidad->nombre}}</td>
                            <td>{{$unidad->descripcion}}</td>
                            <td>{{$unidad->created_at->format('Y-M-d')}}</td>
                            <td>{{$unidad->updated_at->format('Y-M-d')}}</td>
                            <td>
                                <ul class="list-unstyled">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>{{ method_field('get') }}<a href="{{ route('unidad-organizativa.show',$unidad->id) }}"><i class="material-icons">add</i>Subunidad</a></li>
                                            <li><a href="{{ route('unidad-organizativa.edit', $unidad->id) }}"><i class="material-icons">edit</i> Editar</a></li>
                                            <li><a href="javascript:" class="deleteBtn" data-name="{{ $unidad->nombre }}" data-id="{{ $unidad->id }}"><i class="material-icons">delete</i> Eliminar</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
         @endif

            </div>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script type="text/javascript">
        $(function () {
        $('.add').click(function () {
            var n = ($('.resultbody tr').length - 0) + 1;
            var tr = '<tr><td class="no">' + n + '</td>' +
                    '<td><input type="hidden" class="unidadPadre_id form-control"  name="unidadPadre_id[]"  value={{$id}}></td>'+
                    '<td><input type="text" class="nombre form-control" name="nombre[]" value="{{ old('nombre') }}"></td>'+
                    '<td><input type="textarea" class="descripcion form-control" name="descripcion[]"></td>'+
                    '<td><input type="button" class="btn btn-danger delete" value="x"></td></tr>';
            $('.resultbody').append(tr);
        });
        $('.resultbody').delegate('.delete', 'click', function () {
            $(this).parent().parent().remove();
        });
        });
        </script>

@endsection