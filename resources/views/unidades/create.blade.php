@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{url('/')}}"><i class="material-icons">list</i> Unidades Organizativas</a></li>
            <li class="active"><i class="material-icons">archive</i>Agregar Departamento</li>
        </ol>
    </div>
    @if(session()->get('status'))
    <div class="alert alert-success">
        {{ session()->get('status') }}
    </div>
@endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Nuevo Departamento
                    </h2>
                </div>
                <div class="body masked-input">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('unidad-organizativa.save') }}">
                         {!! csrf_field() !!}
                     <table class="table table-striped">
                             <thead>
                                 <tr>
                                     <th>N°</th>
                                     <th>Nombre</th>
                                     <th>Descripcion</th>
                                     <th>Delete</th>
                                 </tr>
                             </thead>
                             <tbody class="resultbody">
                                 <tr>
                                     <td class="no">1</td>
                                     <td>
                                         <input type="text" class="nombre form-control" name="nombre[]" value="{{ old('nombre') }}" autocomplete = "off">
                                     </td>
                                     <td>
                                         <input type="textarea" class="descripcion form-control" name="descripcion[]" value="{{ old('descripcion') }}" autocomplete = "off">
                                     </td>
                                     <td>
                                         <input type="button" class="btn btn-danger delete" value="x">
                                     </td>
                                 </tr>
 
                             </tbody>
                         </table>    
                         <center><input type="button" class="btn btn-lg btn-primary add" value="+ Registro">   
                         <input type="submit" class="btn btn-lg btn-default" value="Guardar"></center>
                         </form>

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.add').click(function () {
            var n = ($('.resultbody tr').length - 0) + 1;
            var tr = '<tr><td class="no">' + n + '</td>' +
                    '<td><input type="text" class="nombre form-control" name="nombre[]" value="{{ old('nombre') }}"></td>'+
                    '<td><input type="textarea" class="descripcion form-control" name="descripcion[]"></td>'+
                    '<td><input type="button" class="btn btn-danger delete" value="x"></td></tr>';
            $('.resultbody').append(tr);
        });
        $('.resultbody').delegate('.delete', 'click', function () {
            $(this).parent().parent().remove();
        });
    });
</script>
@endsection