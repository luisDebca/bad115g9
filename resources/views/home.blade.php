@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="active">
            <i class="material-icons">home</i> Inicio
        </li>
    </ol>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-blue-grey">
                    <h2>
                        Bienvenido {{Auth::user()->name}}
                    </h2>
                </div>
            </div>
        </div>
    </div>
@endsection