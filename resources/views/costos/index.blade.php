@extends('layouts.appTable')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li class="active"><i class="material-icons">archive</i> Costos Asignados</li>
        </ol>
    </div>
    @if(session()->get('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Registros de Montos Asignados a los Departamentos
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('centro-costos.create')}}"><i class="material-icons">add</i> Nuevo</a></li>                                
                                <li><a href="{{route('centro-costos.remove')}}"><i class="material-icons">add</i>Reasignar Monto</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Numero</th>
                                <th>Monto</th>
                                <th>Departamento</th>
                                <th>Accion</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($costos as $costo)
                            @foreach($unidades as $unidad)
                            @if($costo->unidad_id==$unidad->id)
                                <tr>
                                    <td>{{$costo->numero}}</td>
                                    <td>$ {{$costo->monto}}</td>
                                    <td>{{$unidad->nombre}}</td>
                                    <td>
                                        <ul class="list-unstyled">
                                            <li class="dropdown">
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>{{ method_field('get') }}<a href="{{ route('centro-costos.show',$costo->id) }}"><i class="material-icons">details</i> Detalles</a></li>
                                                    <li><a href="{{ route('centro-costos.edit', $costo->id) }}"><i class="material-icons">edit</i> Editar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            @endforeach
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
@endsection