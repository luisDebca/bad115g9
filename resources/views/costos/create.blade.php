@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{url('/')}}"><i class="material-icons">list</i> Unidades Organizativas</a></li>
            <li class="active"><i class="material-icons">archive</i>Agregar Departamento</li>
        </ol>
    </div>
    @if(session()->get('status'))
    <div class="alert alert-success">
        {{ session()->get('status') }}
    </div>
@endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Nuevo Registro de Costos Para los Departamentos.
                    </h2>
                </div>
                <div class="body masked-input">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('centro-costos.store') }}">
                         {!! csrf_field() !!}
                     <table class="table table-striped">
                             <thead>
                                 <tr>
                                    <th>N°</th>
                                    <th>Numero</th>
                                    <th></th>
                                    <th>Departamento</th>
                                    <th>Monto ($)</th>
                                 </tr>
                             </thead>
                             <tbody class="resultbody">
                                 @foreach($unidades as $indexKey => $unidad)
                                 <tr>
                                 <td>{{$indexKey+1}}</td>
                                     <td>
                                     <input type="number" class="form-control" name="numero[]" id="numero" >
                                     </td>
                                     <td>
                                     <input type="hidden" class="form-control" name="unidad_id[]" id="unidad_id " value="{{$unidad->id}}">
                                     </td>
                                     <td>
                                     <input type="text" class="form-control" name="departamento[]" id="departamento" value="{{$unidad->nombre}}">
                                     </td>
                                     <td>
                                         <input type="text" class="form-control" name="monto[]" id="monto" pattern="^[1-9]\d*$">
                                     </td>
                                 </tr>
                                 @endforeach
 
                             </tbody>
                         </table>    
                         <center><input type="submit" class="btn btn-lg btn-default" value="Guardar"></center>
                         </form>
                </div>
            </div>
        </div>
    </div>

@endsection