@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route('puestos.index')}}"><i class="material-icons">list</i> Puestos de trabajo</a></li>
            <li class="active"><i class="material-icons">archive</i> Nuevo puesto</li>
        </ol>
    </div>
    @if ($errors->any())
        <div id="">
            <div class="alert alert-danger">
                <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Formulario para nuevo puesto de trabajo
                    </h2>
                </div>
                <div class="body masked-input">
                    <form id="main" class="form-horizontal" method="post" action="{{route('puestos.store')}}">
                        {{ csrf_field() }}
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="nombre">Nombre del puesto</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="nombre" name="nombre" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="descripcion">Descripción</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea id="descripcion" name="descripcion" class="form-control" placeholder="Breve descripción" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="salarioMaximo">Salario Máximo ($)</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="salarioMaximo" name="salarioMaximo" class="form-control money-dollar" placeholder="Eje: 99,99 $">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="salarioMinimo">Salario Mínimo ($)</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="salarioMinimo" name="salarioMinimo" class="form-control money-dollar" placeholder="Eje: 99,99 $">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>
                                <a href="{{route('puestos.index')}}" type="button" class="btn btn-secondary m-t-15 waves-effect">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $("#main").validate({
            rules: {
                nombre:{
                    required:true,
                    maxlength:50
                },
                descripcion:{
                    maxlength:250
                },
                salarioMaximo:{
                    number:true,
                    greaterThan: "#salarioMinimo"
                },
                salarioMinimo:{
                    number:true,
                    smallerThan: "#salarioMaximo"
                }
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });
    </script>
@endsection
