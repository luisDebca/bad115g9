@extends('layouts.app')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route('puestos.index')}}"><i class="material-icons">list</i> Puestos de trabajo</a></li>
            <li class="active"><i class="material-icons">archive</i> Información</li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Información de puestos de trabajo
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('puestos.edit',$puesto->id)}}"><i class="material-icons">edit</i> Editar</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Nombre del puesto de trabajo</h4>
                            <p class="list-group-item-text">{{ $puesto->nombre }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Descripción</h4>
                            <p class="list-group-item-text">{{ $puesto->descripcion }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Salario Máximo</h4>
                            <p class="list-group-item-text">$ {{ $puesto->salarioMaximo }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Salario Mínimo</h4>
                            <p class="list-group-item-text">$ {{ $puesto->salarioMinimo }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Ultima modificación</h4>
                            <p class="list-group-item-text">{{ $puesto->updated_at->format('Y-M-d H:i:s') }}</p>
                        </div>
                    </div>
                    <div class="row clearfix p-l-15">
                        <div class="btn-group">
                            <a role="button" href="{{ route('puestos.index') }}" class="btn btn-secondary"><i class="material-icons">reply</i> Volver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection