@extends('layouts.appTable')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route('planillas.index')}}"><i class="material-icons">home</i> Planillas</a></li>
            <li class="active"><i class="material-icons">archive</i> Detalle planilla</li>
        </ol>
    </div>
    @if(session()->get('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Modificar planilla de empleados
                    </h2>
                    <small>Modificar los ingresos, descuentos y comisiones para la planilla de empleados</small>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Nombre completo</th>
                                <th>Unidad Organizacional</th>
                                <th>Puesto de trabajo</th>
                                <th>Salario base</th>
                                <th>Ingresos</th>
                                <th>Descuentos</th>
                                <th>Comisiones</th>
                                <th>Sub Total</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($empleados as $emp)
                                <tr>
                                    <td>{{$emp->nombre}}</td>
                                    <td>{{$emp->unidad}}</td>
                                    <td>{{$emp->puesto}}</td>
                                    <td>$ {{$emp->salario}}</td>
                                    <td>$ {{$emp->ingresos + 0.0}}</td>
                                    <td>$ {{$emp->descuentos + 0.0}}</td>
                                    <td>$ {{$emp->comisiones + 0.0}}</td>
                                    <td>$ {{$emp->salario + $emp->ingresos + $emp->comisiones - $emp->descuentos}}</td>
                                    <td>
                                        <ul class="list-unstyled">
                                            <li class="dropdown">
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a class="show-ingresos-btn" data-id="{{$emp->planilla}}" href="javascript:"><i class="material-icons">arrow_upward</i> Ingresos</a></li>
                                                    <li><a class="show-descuentos-btn" data-id="{{$emp->planilla}}" href="javascript:"><i class="material-icons">arrow_downward</i> Descuentos</a></li>
                                                    <li><a class="show-comisiones-btn" data-id="{{$emp->planilla}}" href="javascript:"><i class="material-icons">money</i> Comisiones</a></li>
                                                    <li><a href="javascript:"><i class="material-icons">picture_as_pdf</i> Hoja de pago</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <table class="table table-active">
                    <thead>
                    <tr>
                        <th>Número empleados</th>
                        <th>Total Salario</th>
                        <th>Total Ingresos</th>
                        <th>Total Descuentos</th>
                        <th>Total Comisiones</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{sizeof($empleados)}}</td>
                        <td>$ {{$totalSalario}}</td>
                        <td>$ {{$totalIngresos}}</td>
                        <td>$ {{$totalDescuentos}}</td>
                        <td>$ {{$totalComisiones}}</td>
                        <td>$ {{$totalSalario + $totalIngresos + $totalComisiones - $totalDescuentos}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ingresosModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ingresosModalLabel">Ingresos registrados</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nº</th>
                            <th>Ingreso</th>
                            <th>Valor</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="ingresosBody">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <a id="add-ingreso-btn" href="javascript:" role="button" class="btn btn-link waves-effect">Agregar</a>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="descuentosModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="descuentosModalLabel">Descuentos registrados</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nº</th>
                            <th>Ingreso</th>
                            <th>Valor</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="descuentosBody">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <a id="add-descuento-btn" href="javascript:" role="button" class="btn btn-link waves-effect">Agregar</a>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="comisionesModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="comisionesModalLabel">Comisiones registradas</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nº</th>
                            <th>Ingreso</th>
                            <th>Valor</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="comisionesBody">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <a id="add-comision-btn" href="javascript:" role="button" class="btn btn-link waves-effect">Agregar</a>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).on('click','.show-ingresos-btn', function () {
            var planillaID = $(this).data('id');
            $.ajax({
                url:'../get-planilla-ingresos',
                data:{planilla:planillaID}
            })
                .done(function (data) {
                    if(data.length === 0){
                        $('#ingresosBody').html('<tr class="danger"><td colspan="4">No se encontraron resultados</td></tr>')
                    }else {
                        $('#ingresosBody').empty();
                    }
                    $.each(data, function (key, item) {
                        $('#ingresosBody').append('<tr><td>'+ (key + 1) +'</td><td>'+ item.nombre +'</td><td>'+ item.monto +'</td><td><a class="p-r-5 p-l-5" href="javascript:"><i class="material-icons">edit</i></a><a class="p-l-5 p-r-5" href="../eliminar-planilla-'+ item.planilla +'-ingreso-'+ item.ingreso +'"><i class="material-icons">delete</i></a></td></tr>');
                    });
                });
            $('#add-ingreso-btn').attr('href','../planilla-nuevo-ingreso/' + planillaID);
            $('#ingresosModal').modal('show');
        });
        $(document).on('click','.show-descuentos-btn', function () {
            var planillaID = $(this).data('id');
            $.ajax({
                url:'../get-planilla-descuentos',
                data:{planilla:$(this).data('id')}
            })
                .done(function (data) {
                    if(data.length === 0){
                        $('#descuentosBody').html('<tr class="danger"><td colspan="4">No se encontraron resultados</td></tr>')
                    }else {
                        $('#descuentosBody').empty();
                    }
                    $.each(data, function (key, item) {
                        $('#descuentosBody').append('<tr><td>'+ (key + 1) +'</td><td>'+ item.nombre +'</td><td>'+ item.monto +'</td><td><a class="p-r-5 p-l-5" href="javascript:"><i class="material-icons">edit</i></a><a class="p-l-5 p-r-5" href="../eliminar-planilla-'+ item.planilla +'-descuento-'+ item.ingreso +'"><i class="material-icons">delete</i></a></td></tr>');
                    });
                });
            $('#add-descuento-btn').attr('href','../planilla-nuevo-descuento/' + planillaID);
            $('#descuentosModal').modal('show');
        });
        $(document).on('click','.show-comisiones-btn', function () {
            var planillaID = $(this).data('id');
            $.ajax({
                url:'../get-planilla-comisiones',
                data:{planilla:$(this).data('id')}
            })
                .done(function (data) {
                    if(data.length === 0){
                        $('#comisionesBody').html('<tr class="danger"><td colspan="4">No se encontraron resultados</td></tr>')
                    }else {
                        $('#comisionesBody').empty();
                    }
                    $.each(data, function (key, item) {
                        $('#comisionesBody').append('<tr><td>'+ (key + 1) +'</td><td>'+ item.nombre +'</td><td>'+ item.monto +'</td><td><a class="p-r-5 p-l-5" href="javascript:"><i class="material-icons">edit</i></a><a class="p-l-5 p-r-5" href="../eliminar-planilla-'+ item.planilla +'-comision-'+ item.ingreso +'"><i class="material-icons">delete</i></a></td></tr>');
                    });
                });
            $('#add-comision-btn').attr('href','../planilla-nuevo-comision/' + planillaID);
            $('#comisionesModal').modal('show');
        });
    </script>
@endsection