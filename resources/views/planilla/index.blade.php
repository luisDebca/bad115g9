@extends('layouts.appTable')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li class="active"><i class="material-icons">archive</i> Planilla</li>
        </ol>
    </div>
    <div id="info-panel">
        @if(session()->get('message'))
            <div class="alert alert-success" >
                {{ session()->get('message') }}
            </div>
        @endif
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Planillas de empleados
                    </h2>
                    <small>Listado de planilla de empleados generadas</small>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a id="nueva-planilla-btn" href="javascript:"><i class="material-icons">add</i> Nuevo</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Número planilla</th>
                                <th>Fecha inicio</th>
                                <th>Fecha fin</th>
                                <th>Empleados</th>
                                <th>Total</th>
                                <th>Ingresos</th>
                                <th>Descuentos</th>
                                <th>Comisiones</th>
                                <th>Estado</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($planillas as $planilla)
                                <tr>
                                    <td>P-{{$planilla->numero}}</td>
                                    <td>{{$planilla->inicio}}</td>
                                    <td>{{$planilla->fin}}</td>
                                    <td>{{$planilla->empleados + 0}}</td>
                                    <td>$ {{$planilla->total + 0.0}}</td>
                                    <td>$ {{$planilla->ingresos + 0.0}}</td>
                                    <td>$ {{$planilla->descuentos + 0.0}}</td>
                                    <td>$ {{$planilla->comisiones + 0.0}}</td>
                                    @if($planilla->validado)
                                        <td class="bg-green text-center"><i class="material-icons">check_circle_outline</i><br> Validada</td>
                                        @else
                                        <td class="bg-danger text-center"><i class="material-icons">warning</i><br> Sin validar</td>
                                    @endif
                                    <td>
                                        <ul class="list-unstyled">
                                            <li class="dropdown">
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="{{url('planilla-modificar-empleados',$planilla->numero)}}" class="showInfo"><i class="material-icons">details</i> Ver planilla</a></li>
                                                    <li><a href="{{ url('planilla-editar', $planilla->numero) }}"><i class="material-icons">edit</i> Editar</a></li>
                                                    <li><a href="javascript:" class="validar-btn" data-numero="{{$planilla->numero}}"><i class="material-icons">donut_large</i> Validar</a></li>
                                                    <li><a href="{{url('planilla-reporte-empleados',$planilla->numero)}}" class="showInfo"><i class="material-icons">picture_as_pdf</i> Generar PDF</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="departamentos-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Seleccione los departamentos para nueva planilla</h4>
                </div>
                <form method="post" action="{{url('planilla-seleccionar-unidades')}}">
                    {{ method_field('POST') }}
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">Seleccionado</th>
                                <th>Nombre departamento</th>
                            </tr>
                            </thead>
                            <tbody id="departamento-table-body">
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button id="check-all" type="button" class="btn btn-link waves-effect" hidden="hidden">Agregar todos</button>
                        <button type="submit" class="btn btn-link waves-effect">Iniciar planilla</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="costos-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="costoLabel">Departamentos afectados</h4>
                </div>
                <div class="modal-body">
                    <h5>Los siguientes departamentos son afectados en sus montos para el año actual.</h5>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Unidad Organizacional</th>
                            <th>Monto actual</th>
                        </tr>
                        </thead>
                        <tbody id="costo-body">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">Aplicar cambios planilla</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        var url = '/get-departamentos-planilla';
        $("#check-all").hide();

        $('#nueva-planilla-btn').on('click',function () {
            $('#departamento-table-body').empty();
            $.ajax(url)
                .done(function (data) {
                    if (data.length === 0){
                        $('#departamento-table-body').append('<tr><td class="text-center">No se encontraron departamentos</td></tr>');
                    }
                    $.each(data,function (key, item) {
                        $('#departamento-table-body').append('<tr><td class="text-center"><input class="check-dep" type="checkbox" id="'+ item.id +'" name="departamentos[]" value="'+ item.id +'"> </td><td>'+ item.nombre +'</td></tr>');
                    });
                    $("#check-all").show();
                    $('#departamentos-modal').modal('show');
                });
        });

        $("#check-all").on("click",function () {
           $(".check-dep").prop('checked', true);
        });

        $(document).on('click','.validar-btn', function () {
            var body = $('#costo-body');
            $.ajax({
                url:'planilla-centros-costos',
                data: {numero:$(this).data('numero')}
            }).done(function (data) {
                body.empty();
                if (data.length == 0) {
                    body.append('<tr><td colspan="2" class="bg-warning">No se encontraron resultados</td></tr>');
                }
               $.each(data, function (key, item) {
                   body.append('<tr><td>'+ item.nombre +'</td><td>$'+ item.monto +'</td></tr>');
               });
            });
           $('#costos-modal').modal("show");
        });
    </script>
@endsection