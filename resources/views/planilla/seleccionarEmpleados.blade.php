@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route('planillas.index')}}"><i class="material-icons">home</i> Planilla</a></li>
            <li class="active"><i class="material-icons">archive</i> Seleccionar empleados</li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Empleados para nueva planilla
                    </h2>
                    <small>Seleccione los empleados que contendra la nueva planilla.</small>
                </div>
                <div class="body">
                    <form method="post" action="{{url('planilla-seleccionar-empleados')}}">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}
                        <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                            @foreach($departamentos as $dep)
                                <div class="panel panel-primary">
                                    <div class="panel-heading" role="tab" id="headingOne_{{$dep->id}}">
                                        <h4 class="panel-title">
                                            <a class="unidad-panel" role="button" data-toggle="collapse" data-parent="#accordion_{{$dep->id}}" data-id="{{$dep->id}}" href="#collapseOne_{{$dep->id}}" aria-expanded="true" aria-controls="collapseOne_{{$dep->id}}">
                                                <i class="material-icons">inbox</i> {{$dep->nombre}}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne_{{$dep->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_{{$dep->id}}">
                                        <div class="panel-body"></div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button class="btn btn-primary" type="submit">Crear planilla</button>
                        <a class="btn btn-secondary" role="button" href="{{route('planillas.index')}}">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $(document).on('click','.unidad-panel',function () {
           var body = $(this).parent().parent().parent().find(".panel-body");
           var id = $(this).data('id');
           if (body.html().length < 10){
               body.html('<div class="panel-group" id="accordion_'+ (id*100 + 1) +'" role="tablist" aria-multiselectable="true"></div>');
               $.ajax({url:'get-sub-unidades-planilla',data:{padre:id}})
                   .done(function (data) {
                       if (data.length === 0) {$("#accordion_" + (id*100 + 1)).append('<div class="alert alert-info">No se encontraron sub-unidades para esta unidad.</div>');}
                      $.each(data, function (key, item) {
                         $("#accordion_" + (id*100 + 1)).append('<div class="panel panel-primary"><div class="panel-heading" role="tab" id="headingOne_'+ item.id +'"><h4 class="panel-title"><a class="unidad-panel" role="button" data-toggle="collapse" data-parent="#accordion_'+ item.id +'" data-id="'+ item.id +'" href="#collapseOne_'+ item.id +'" aria-expanded="true" aria-controls="collapseOne_'+ item.id +'"><i class="material-icons">add</i> '+ item.nombre +'</a></h4></div><div id="collapseOne_'+ item.id +'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_'+ item.id +'"><div class="panel-body"></div></div></div>');
                      });
                      $.ajax({url:'get-empleados-planilla',data:{unidad:id}})
                          .done(function (emps) {
                            if (emps.length > 0) {
                                $("#accordion_" + (id*100 + 1)).append('<table id="table-emp" class="table table-hover"><tbody></tbody></table>');
                                $.each(emps, function (index, emp) {
                                    $("#accordion_" + (id*100 + 1)).find('#table-emp tbody').append('<tr><td class="text-center"><input type="checkbox" id="emp-'+ emp.id +'" name="empleados[]" value="'+ emp.id +'"></td><td>'+ emp.nombre +'</td></tr>');
                                })
                            }else {
                                $("#accordion_" + (id*100 + 1)).append('<div class="alert alert-info">No se encontraron empleados asignados a esta unidad.</div>');
                            }
                          });
                   });
           } else {
               body.empty();
           }
        });
    </script>
@endsection