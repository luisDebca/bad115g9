@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route( 'planillas.index')}}"><i class="material-icons">list</i> Planillas</a></li>
            <li><a href="{{url( 'planilla-modificar-empleados', $numero)}}"><i class="material-icons">list</i> Detalle planilla</a></li>
            <li class="active"><i class="material-icons">archive</i> Descuentos a planilla</li>
        </ol>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Descuento a planilla
                    </h2>
                    <small>Agregar descuento de empleados a planilla</small>
                </div>
                <div class="body masked-input">
                    <form id="main" class="form-horizontal" method="post" action="{{url('planilla-nuevo-descuento',$planilla)}}">
                        {{ csrf_field() }}
                        <input class="form-control hidden" name="planilla" value="{{$planilla}}">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="ingreso">Seleccione el tipo de descuento</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select id="ingreso" name="tipo" class="form-control recalcular">
                                            @foreach($descuentos as $in)
                                                <option value="{{$in->id}}">{{$in->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input class="form-control hidden" type="text" name="monto" id="monto">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="estado">Ingreso Activo</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input id="estado" type="checkbox" name="estado" checked>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="desde">Validez</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="input-daterange input-group" id="bs_datepicker_range_container">
                                    <div class="form-line">
                                        <input type="text" name="desde" class="form-control" placeholder="Desde...">
                                    </div>
                                    <span class="input-group-addon">a</span>
                                    <div class="form-line">
                                        <input type="text" name="hasta" class="form-control" placeholder="Hasta...">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="desde">Ingreso Total</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="p-l-15 p-r-15 p-t-0 p-b-15">
                                    <h1 id="calculo">$0.00</h1>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>
                                <a href="{{url('planilla-modificar-empleados',$numero)}}" type="button" class="btn btn-secondary m-t-15 waves-effect">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div hidden="hidden" id="salario">{{$salario}}</div>
    <div hidden="hidden">
        @foreach($descuentos as $ing)
            <div id="ingreso-{{$ing->id}}" data-tipo="{{$ing->tipo}}" data-tasa="{{$ing->taza}}" data-monto="{{$ing->montoFijo}}"></div>
        @endforeach
    </div>
@endsection
@section('javascript')
    <script>
        $(document).ready(function () {
           calcularMonto();
        });
        $( "#main" ).validate({
            rules: {
                tipo:{
                    required:true
                },
                monto:{
                    required:true,
                    number:true
                },
                estado:{
                    required:true
                },
                desde:{
                    required:true
                },
                hasta:{
                    required:true
                }
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });
        $('#bs_datepicker_range_container').datepicker({
            autoclose: true,
            container: '#bs_datepicker_range_container',
            format: 'yyyy-mm-dd'
        });
        $(".recalcular").on('input change', function () {
            calcularMonto()
        });
        function calcularMonto() {
            var salario = $('#salario').text();
            var ingreso = $('#ingreso-' + $("#ingreso").val());
            var resultado = 0.0;
            if (ingreso.data('tipo') === 'BASE'){
                resultado = Math.round((salario * (ingreso.data('tasa')/100))*100)/100;
            }else if (ingreso.data('tipo') === 'FIJO'){
                resultado = ingreso.data('monto');
            }
            $("#monto").val(resultado);
            $("#calculo").text('$' + resultado);
        }
    </script>
@endsection