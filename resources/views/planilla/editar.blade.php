@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route( 'planillas.index')}}"><i class="material-icons">list</i> Planillas</a></li>
            <li class="active"><i class="material-icons">archive</i> Editar planilla</li>
        </ol>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Formulario para editar planilla
                    </h2>
                    <small>Planilla de empleados</small>
                </div>
                <div class="body masked-input">
                    <form id="main" class="form-horizontal" method="post" action="{{url('planilla-actualizar', $planilla->numero)}}">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}
                        <input name="numero" class="hidden" value="{{$planilla->numero}}">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="numeroDef">Número de planilla</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="numeroDef" name="numeroDef" class="form-control" disabled="disabled" value="{{ $planilla->numero }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="fechaInicio">Fechas de planilla</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="input-daterange input-group" id="bs_datepicker_range_container">
                                    <div class="form-line">
                                        <input type="text" id="fechaInicio" name="fechaInicio" class="form-control" placeholder="Inicio..." value="{{$planilla->fechaInicio}}">
                                    </div>
                                    <span class="input-group-addon">a</span>
                                    <div class="form-line">
                                        <input type="text" id="fechaFinal" name="fechaFinal" class="form-control" placeholder="Fin..." value="{{$planilla->fechaFinal}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Actualizar</button>
                                <a href="{{route('planillas.index')}}" type="button" class="btn btn-secondary m-t-15 waves-effect">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $( "#main" ).validate({
            rules: {
                fechaInicio: {
                    required: true
                },
                fechaFinal: {
                    required: true
                }
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });
        $('#bs_datepicker_range_container').datepicker({
            autoclose: true,
            container: '#bs_datepicker_range_container',
            format: 'yyyy-mm-dd'
        });
    </script>
@endsection