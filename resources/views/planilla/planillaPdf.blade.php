  
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Planilla</title>
  <link href="{{ public_path()}}/plantilla/estiloreportes.css" rel="stylesheet">   
</head>
<body>
    <script type="text/php">
        if ( isset($pdf) ) {
        // OLD 
        // $font = Font_Metrics::get_font("helvetica", "bold");
        // $pdf->page_text(72, 18, "{PAGE_NUM} of {PAGE_COUNT}", $font, 6, array(255,0,0));
        // v.0.7.0 and greater
        $x = 695;
        $y = 575;
        $text = "Pagina {PAGE_NUM} de {PAGE_COUNT}";
        $font = $fontMetrics->get_font("helvetica", "bold");
        $size = 8;
        $color = array(0,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>
<div class="page-break">
    <!--Contenido -->
    <div>
        <header>
         <div id="logo" >
             <img src="{{public_path().'/images/'.$empresa->logoDir}}" height="100" width="100" style="float: left; overflow: auto">
        </div>
        <div style="text-align: center" class="encabezado" id="encabezado">
            <h2 style="">{{$empresa->nombre}}</h2>
            <h2 style="">Unidad Organizacional</h2>
            <br>
            <h5 style="">Generado: {{date("d-m-Y G:i:s")}} por: {{Auth::user()->name}}</h5>
        </div>

        
    </header>
</div>
<div class="panel-heading">
    <br>
    <br>            
    <table WIDTH=100% class="table table-striped table-bordered" style="">
            <thead>
                            <tr>
                                <th>Nombre completo</th>
                                <th>Unidad Organizacional</th>
                                <th>Puesto de trabajo</th>
                                <th>Salario base</th>
                                <th>Ingresos/th>
                                <th>Comisiones</th>
                                <th>Descuentos</th>
                                <th>Totales</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($empleados as $emp)
                                <tr>
                                    <td>{{$emp->nombre}}</td>
                                    <td>{{$emp->unidad}}</td>
                                    <td>{{$emp->puesto}}</td>
                                    <td>$ {{$emp->salario}}</td>
                                    <td>$ {{$emp->ingresos + 0.0}}</td>
                                    <td>$ {{$emp->comisiones + 0.0}}</td>
                                    <td>$ {{$emp->descuentos + 0.0}}</td>
                                    <td>$ {{$emp->salario + $emp->ingresos + $emp->comisiones - $emp->descuentos}}</td>
                                </tr>
                            @endforeach
                            </tbody>
        <tfoot>
        <tr>
            <td colspan="3" style="text-align: center">Totales</td>
            <td>$ {{$totalSalario}}</td>
            <td>$ {{$totalIngresos}}</td>
            <td>$ {{$totalComisiones}}</td>
            <td>$ {{$totalDescuentos}}</td>
            <td>$ {{$totalSalario + $totalIngresos +$totalComisiones - $totalDescuentos}}</td>
        </tr>
        </tfoot>
                        </table>    </div>
</div>
</div>              
<!-- Fin Contenido-->
</div>
</body>
</html> 