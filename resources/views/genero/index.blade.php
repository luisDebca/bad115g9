@extends('layouts.appTable')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li class="active"><i class="material-icons">archive</i> Generos</li>
        </ol>
    </div>
     @if(session()->get('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Generos
                    </h2>
                   
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                              </a>
                                <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('genero.create')}}"><i class="material-icons">add</i> Nuevo Genero</a></li>
                                </ul>
                    
                        </li>
                    </ul>

                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Tipo</th>
                            </tr>
                            </thead>
                            <tbody>
               @if($gen->count())
              @foreach($gen as $genero)  
              <tr>
                <td>{{$genero->tipo}}</td>
                <td>
                  <ul class="list-unstyled">
                                            <li class="dropdown">
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                              
                                                    <li><a href="{{ route('genero.edit', $genero->id) }}"><i class="material-icons">edit</i> Editar</a></li>
                                                    <li><a href="javascript:" class="deleteBtn" data-name="{{ $genero->tipo }}" data-id="{{ $genero->id }}"><i class="material-icons">delete</i> Eliminar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
            
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="delete-form" method="post" action="javascript:">
                    {{ method_field('delete') }}
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Eliminar tipo de genero</h4>
                    </div>
                    <div class="modal-body">
                        Se eliminara el genero de <strong id="to-delete-name"></strong>
                        <p>¿Desea continuar?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger waves-effect">Eliminar</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(".deleteBtn").on('click',function () {
            var id = $(this).data('id');
            var name = $(this).data('name');
            $("#to-delete-name").text(name);
            $("#delete-form").attr('action',window.location.href + '/' + id);
            $("#deleteModal").modal("show");
        });
    </script>
@endsection