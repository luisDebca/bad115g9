@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route( 'descuentos.index')}}"><i class="material-icons">list</i> Descuentos</a></li>
            <li class="active"><i class="material-icons">archive</i> Nuevo descuento</li>
        </ol>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Formulario para nuevo descuento
                    </h2>
                </div>
                <div class="body masked-input">
                    <form id="main" class="form-horizontal" method="post" action="{{route('descuentos.store')}}">
                        {{ csrf_field() }}
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="nombre">Descuento</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="nombre" name="nombre" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="tipo">Tipo</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select id="tipo" name="tipo" class="form-control">
                                            <option value="BASE">En base al salario</option>
                                            <option value="FIJO">Monto Fijo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="taza">Tasa</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="taza" name="taza" class="form-control porcent" placeholder="Eje: 10.00 %">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="montoFijo">Valor Fijo</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="montoFijo" name="montoFijo" class="form-control money-dollar" placeholder="Eje: 99.99 " disabled="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>
                                <a href="{{route('descuentos.index')}}" type="button" class="btn btn-secondary m-t-15 waves-effect">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $( "#main" ).validate({
            rules: {
                nombre: {
                    required: true,
                    maxlength: 50
                },
                taza: {
                    required: true,
                    number: true,
                    range: [0,100]
                },
                tipo:{
                    required: true
                },
                montoFijo:{
                    required: true,
                    number: true
                }
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });
        $("#tipo").on("change", function () {
            var tasaInput = $("#taza");
            var montoInput = $("#montoFijo");
            if ($(this).val() === 'BASE'){
                tasaInput.removeAttr('disabled');
                montoInput.attr('disabled','');
                montoInput.val('0.00')
            } else if ($(this).val() === 'FIJO'){
                montoInput.removeAttr('disabled');
                tasaInput.attr('disabled','');
                tasaInput.val('0.00')
            }
        });
    </script>
@endsection