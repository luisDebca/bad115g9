@extends('layouts.app')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route('rentas.index')}}"><i class="material-icons">list</i> Tabla de retenciones</a></li>
            <li class="active"><i class="material-icons">archive</i> Información</li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Información del Tramo {{$renta->tramo}}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('rentas.edit',$renta->id)}}"><i class="material-icons">edit</i> Editar</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Desde</h4>
                            <p class="list-group-item-text">$ {{ $renta->desde }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Hasta</h4>
                            <p class="list-group-item-text">$ {{ $renta->hasta }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Porcentaje a aplicar</h4>
                            <p class="list-group-item-text">% {{ $renta->porcentaje }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Sobre el exceso de</h4>
                            <p class="list-group-item-text">$ {{ $renta->sobreExceso }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Más cuota fija de</h4>
                            <p class="list-group-item-text">{{ $renta->cuotaFija }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Ultima modificación</h4>
                            <p class="list-group-item-text">{{ $renta->updated_at->format('Y-M-d H:i:s') }}</p>
                        </div>
                    </div>
                    <div class="row clearfix p-l-15">
                        <div class="btn-group">
                            <a role="button" href="{{ route('rentas.index') }}" class="btn btn-secondary"><i class="material-icons">reply</i> Volver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection