@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route('rentas.index')}}"><i class="material-icons">list</i> Tabla de renta</a></li>
            <li class="active"><i class="material-icons">archive</i> Nueva renta</li>
        </ol>
    </div>
    @if ($errors->any())
        <div id="alert-panel">
            <div class="alert alert-danger">
                <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Formulario para tramo en tabla de renta
                    </h2>
                    <small>La tabla de retención debe estar en concordancia con la presentada por el ministerio de hacienda</small>
                </div>
                <div class="body masked-input">
                    <form id="main" class="form-horizontal" method="post" action="{{route('rentas.store')}}">
                        {{ csrf_field() }}
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="tramo">Tramo</label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" id="tramo" name="tramo" class="form-control" max="100" min="1">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="desde">Desde ($)</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="desde" name="desde" class="form-control money-dollar" placeholder="Eje: 99,99 $">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="hasta">Hasta ($)</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="hasta" name="hasta" class="form-control money-dollar" placeholder="Eje: 99,99 $">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="aplicaRetencion">Aplicar retención</label>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <div class="switch">
                                    <input class="filled-in" type="checkbox" id="aplicaRetencion" name="aplicaRetencion" value="aplicaRetencion" checked>
                                    <input class="form-control hidden" value="true" type="text" id="retencion" name="retencion">
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="porcentaje">Porcentaje a aplicar (%)</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="porcentaje" name="porcentaje" class="form-control porcent valor-retenido" placeholder="Eje: 5.00 %">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="sobreExceso">Sobre el exceso de ($)</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="sobreExceso" name="sobreExceso" class="form-control money-dollar valor-retenido" placeholder="Eje: 99,99 $">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="cuotaFija">Más cuota fija de ($)</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="cuotaFija" name="cuotaFija" class="form-control money-dollar valor-retenido" placeholder="Eje: 99,99 $">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>
                                <a href="{{route('rentas.index')}}" type="button" class="btn btn-secondary m-t-15 waves-effect">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $("#main").validate({
            rules: {
                tramo:{
                    required:true,
                    number:true,
                    range: [1,100]
                },
                hasta:{
                    number:true,
                    greaterThan: "#desde"
                },
                desde:{
                    number:true,
                    smallerThan: "#hasta"
                },
                porcentaje:{
                    required: true,
                    number:true
                },
                sobreExceso: {
                    required:true,
                    number:true
                },
                cuotaFija:{
                    required:true,
                    number:true
                }
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });
        $("#aplicaRetencion").on("change", function () {
            var valoresRetenidos = $(".valor-retenido");
            if ($(this).is(':checked')){
                valoresRetenidos.removeAttr("disabled");
                $("#retencion").val(true);
            } else {
                valoresRetenidos.attr("disabled","");
                valoresRetenidos.val("0.00");
                $("#retencion").val(false);
            }
        })
    </script>
@endsection
