@extends('layouts.appTable')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li class="active"><i class="material-icons">archive</i> Renta</li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Tablas de retención del impuesto sobre la renta
                    </h2>
                    <small>La tabla de retención debe estar en concordancia con la presentada por el ministerio de hacienda</small>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('rentas.create')}}"><i class="material-icons">add</i> Nuevo</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Tramo</th>
                                <th>Desde</th>
                                <th>Hasta</th>
                                <th>% a aplicar</th>
                                <th>Sobre el exceso de</th>
                                <th>Más cuota fija de</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rentas as $renta)
                                <tr>
                                    <td>TRAMO {{$renta->tramo}}</td>
                                    <td>$ {{$renta->desde}}</td>
                                    <td>$ {{$renta->hasta}}</td>
                                    @if($renta->retencion)
                                        <td>{{$renta->porcentaje}} %</td>
                                        <td>$ {{$renta->sobreExceso}}</td>
                                        <td>$ {{$renta->cuotaFija}}</td>
                                    @else
                                        <td class="text-center">SIN RETENCIÓN</td>
                                        <td class="text-center">SIN RETENCIÓN</td>
                                        <td class="text-center">SIN RETENCIÓN</td>
                                    @endif
                                    <td>
                                        <ul class="list-unstyled">
                                            <li class="dropdown">
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>{{ method_field('get') }}<a href="{{ route('rentas.show',$renta->id) }}"><i class="material-icons">details</i> Detalles</a></li>
                                                    <li><a href="{{ route('rentas.edit', $renta->id) }}"><i class="material-icons">edit</i> Editar</a></li>
                                                    <li><a href="javascript:" class="deleteBtn" data-name="{{ $renta->tramo }}" data-id="{{ $renta->id }}"><i class="material-icons">delete</i> Eliminar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="delete-form" method="post" action="javascript:">
                    {{ method_field('delete') }}
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Eliminar de la retención</h4>
                    </div>
                    <div class="modal-body">
                        Se eliminara el tramo <strong id="to-delete-name"></strong>
                        <p>¿Desea continuar?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger waves-effect">Eliminar</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $(document).on('click','.deleteBtn',function () {
            var id = $(this).data('id');
            var name = $(this).data('name');
            $("#to-delete-name").text(name);
            $("#delete-form").attr('action',window.location.href + '/' + id);
            $("#deleteModal").modal("show");
        });

        $(document).on('click','.showInfo',function (e) {
            e.preventDefault();
            var url = $(this).attr("href");
            var result = $.ajax(url)
                .done(function (data) {
                    $(".modal-body #info-name .info-text").text(data.nombre);
                    $(".modal-body #info-tasa .info-text").text(data.taza);
                    $(".modal-body #info-created .info-text").text(data.created_at);
                    $(".modal-body #info-updated .info-text").text(data.updated_at);
                }).fail(function () {
                    $("#info-panel").append('<div class="alert alert-danger alert-dismissable" role="alert"><button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button> No se puede obtener la información del servidor, intente denuevo más tarde.</div>')
                });
            $("#infoModal").modal('show');
        });
    </script>
@endsection