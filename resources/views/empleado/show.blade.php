@extends('layouts.app')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route('empleado.index')}}"><i class="material-icons">list</i> Empleados</a></li>
            <li class="active"><i class="material-icons">archive</i> Información</li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Ficha de Empleado
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('empleado.edit',$empleado->id)}}"><i class="material-icons">edit</i> Editar</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Código de empleado</h4>
                            <p class="list-group-item-text">{{ $empleado->codigo }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Primer Nombre</h4>
                            <p class="list-group-item-text">{{ $empleado->primerNombre }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Segundo Nombre</h4>
                            <p class="list-group-item-text">{{ $empleado->segundoNombre }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Apellido Paterno</h4>
                            <p class="list-group-item-text">{{ $empleado->apellidoPaterno }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Apellido Materno</h4>
                            <p class="list-group-item-text">{{ $empleado->apellidoMaterno }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Apellido Casada</h4>
                            <p class="list-group-item-text">{{ $empleado->apellidoCasada }}</p>
                        </div>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Fecha de nacimiento</h4>
                            <p class="list-group-item-text">{{ $empleado->fechaNacimiento }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Documento unico de identidad</h4>
                            <p class="list-group-item-text">{{ $empleado->dui }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Numero de identificacion tributaria</h4>
                            <p class="list-group-item-text">{{ $empleado->nit }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Número de ISSS</h4>
                            <p class="list-group-item-text">{{ $empleado->isss}}</p>
                        </div>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Numero unico de pension</h4>
                            <p class="list-group-item-text">{{ $empleado->nup }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Pasaporte</h4>
                            <p class="list-group-item-text">{{ $empleado->pasaporte }}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Email Personal</h4>
                            <p class="list-group-item-text">{{ $empleado->emailPersonal}}</p>
                        </div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Email Institucional</h4>
                            <p class="list-group-item-text">{{ $empleado->emailInstitucional}}</p>
                        </div>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Genero</h4>
                    <p class="list-group-item-text">{{$genem->tipo}}</p>
                        </div>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Estado Civil</h4>
                            <p class="list-group-item-text">{{ $estc->tipo}}</p>
                        </div>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Profesion</h4>
                            <p class="list-group-item-text">{{ $prof->nombre}}</p>
                        </div>
                    </div>

                    <div class="list-group">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Jefe</h4>
                            <p class="list-group-item-text">{{ $jef->primerNombre}}</p>
                        </div>
                    </div>






                    <div class="row clearfix p-l-15">
                        <div class="btn-group">
                            <a role="button" href="{{ route('empleado.index') }}" class="btn btn-secondary"><i class="material-icons">reply</i> Volver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection