@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route( 'empleado.index')}}"><i class="material-icons">list</i> Empleados</a></li>
            <li class="active"><i class="material-icons">archive</i> Editar empleado</li>
        </ol>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Formulario para editar Empleado
                    </h2>
                </div>
                <div class="body masked-input">
                    <form class="form-horizontal" id="main" method="post" action="{{route('empleado.update',$empleado->id)}}">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="codigo">Codigo</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="codigo" name="codigo" class="form-control" value="{{ $empleado->codigo }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="primerNombre">Primer Nombre</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="primerNombre" name="primerNombre" class="form-control" value="{{ $empleado->primerNombre }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="segundoNombre">Segundo Nombre</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="segundoNombre" name="segundoNombre" class="form-control" value="{{ $empleado->segundoNombre }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="apellidoPaterno">Apellido Paterno</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="apellidoPaterno" name="apellidoPaterno" class="form-control" value="{{ $empleado->apellidoPaterno }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="apellidoMaterno">Apellido Materno</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="apellidoMaterno" name="apellidoMaterno" class="form-control" value="{{ $empleado->apellidoMaterno }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="apellidoCasada">Apellido de casada</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="apellidoCasada" name="apellidoCasada" class="form-control" value="{{ $empleado->apellidoCasada }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="fechaNacimiento">Fecha de nacimiento</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="date" id="fechaNacimiento" name="fechaNacimiento" class="form-control" value="{{ $empleado->fechaNacimiento }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="dui">DUI</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="dui" name="dui" class="form-control" value="{{ $empleado->dui }}"
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="nit">NIT</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="nit" name="nit" class="form-control" value="{{ $empleado->nit }}"
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="isss">Numero ISSS</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="isss" name="isss" class="form-control" value="{{ $empleado->isss }}"
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="nup">NUP</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="nup" name="nup" class="form-control" value="{{ $empleado->nup }}"
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="pasaporte">Pasaporte</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="pasaporte" name="pasaporte" class="form-control" value="{{ $empleado->pasaporte }}"
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="emailPersonal">email personal</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" id="emailPersonal" name="emailPersonal" class="form-control" value="{{ $empleado->emailPersonal }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="emailInstitucional">email institucional</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" id="emailInstitucional" name="emailInstitucional" class="form-control" value="{{ $empleado->emailInstitucional }}"
                                       >
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="genero_id">Genero</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                       <select name= "genero_id" id="genero_id" class="form-control">
                                 <option selected value="" >Seleccione una opcion</option>
                                  @foreach ($gen as $gen)
                                   <option value="{{$gen['id']}}">{{$gen['tipo']}}
                                 </option>
                                   @endforeach 
                                </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="estadoCivil_id">Estado Civil</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                       <select name= "estadoCivil_id" id="estadoCivil_id" class="form-control">
                                 <option selected value="" >Seleccione una opcion</option>
                                  @foreach ($estadoCivil as $es)
                                   <option value="{{$es['id']}}">{{$es['tipo']}}
                                 </option>
                                   @endforeach 
                                </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="profesion_id">Profesion</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                       <select name= "profesion_id" id="profesion_id" class="form-control">
                                 <option selected value="" >Seleccione una opcion</option>
                                  @foreach ($profe as $pro)
                                   <option value="{{$pro['id']}}">{{$pro['nombre']}}
                                 </option>
                                   @endforeach 
                                </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                         <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="jefe_id">Jefe</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                       <select name= "jefe_id" id="jefe_id" class="form-control">
                                 <option selected value="" >Seleccione una opcion</option>
                                  @foreach ($jefe as $je)
                                   <option value="{{$je['id']}}">{{$je['primerNombre']}}
                                 </option>
                                   @endforeach 
                                </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Actualizar</button>
                                <a href="{{route('empleado.index')}}" type="button" class="btn btn-secondary m-t-15 waves-effect">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $( "#main" ).validate({
            rules: {
                codigo:{
                    required: true,
                    maxlength:15
                },
                primerNombre:{
                    required:true,
                    maxlength:50
                },
                segundoNombre:{
                    required:true,
                    maxlength:50
                },
                apellidoPaterno:{
                    required: true,
                    maxlength: 50
                },
                apellidoMaterno: {
                    required:true,
                    maxlength: 50
                },
                apellidoCasada: {
                    maxlength:50
                },
                dui:{
                    required: true,
                    maxlength:10
                },
                nit:{
                    required:true,
                    maxlength:true
                },
                isss:{
                    required:true,
                    maxlength:10
                },
                nup:{
                    required:true,
                    maxlength:12
                },
                pasaporte:{
                    maxlength:10
                },
                emailPersonal:{
                    email:true
                },
                emailInstitucional:{
                    email:true
                },
                genero_id:{
                    required:true
                },
                estadoCivil_id:{
                    required:true
                },
                profesion_id:{
                    required:true
                },
                
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });
        $('#bs_datepicker_container input').datepicker({
            autoclose: true,
            container: '#bs_datepicker_container'
        });
@endsection
