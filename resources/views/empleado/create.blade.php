@extends('layouts.appForm')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li><a href="{{route( 'empleado.index')}}"><i class="material-icons">list</i> Empleados</a></li>
            <li class="active"><i class="material-icons">archive</i> Nuevo</li>
        </ol>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <h4><i class="material-icons">error</i> Se encontraron los siguientes errores</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Formulario para ingresar un nuevo Empleado
                    </h2>
                </div>

                <div class="body masked-input">

                    <form id="main" class="form-horizontal" method="post" action="{{route('empleado.store')}}">
                        {{ csrf_field() }}
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="nombre">Codigo</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="codigo" name="codigo" class="form-control" placeholder="Ejemplo EM123">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="primerNombre">Primer Nombre</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="primerNombre" name="primerNombre" class="form-control" placeholder="Ejemplo Juan">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="segundoNombre">Segundo Nombre</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="segundoNombre" name="segundoNombre" class="form-control" placeholder="Ejemplo Carlos">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="apellidoPaterno">Apellido Paterno</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="apellidoPaterno" name="apellidoPaterno" class="form-control" placeholder="Apellido Paterno">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="apellidoMaterno">Apellido Materno</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="apellidoMaterno" name="apellidoMaterno" class="form-control" placeholder="Apellido Materno">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="apellidoCasada">Apellido de casada</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="apellidoCasada" name="apellidoCasada" class="form-control" placeholder="Apellido de casada">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="fechaNacimiento">Fecha de nacimiento</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <div class="form-line" id="bs_datepicker_container">
                                            <input type="text" class="form-control" placeholder="Seleccione una fecha...">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="dui">DUI</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="dui" name="dui" class="form-control dui" placeholder="00000000-0">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="nit">NIT</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="nit" name="nit" class="form-control nit" placeholder="0000-000000-000-0">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="isss">Numero ISSS</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="isss" name="isss" class="form-control isss" placeholder="000000000">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="nup">NUP</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="nup" name="nup" class="form-control nup" placeholder="000000000000">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="pasaporte">Pasaporte</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="pasaporte" name="pasaporte" class="form-control" placeholder="D0000000">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="emailPersonal">email personal</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="emailPersonal" name="emailPersonal" class="form-control email" placeholder="nombre@gmail.com">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="emailInstitucional">email institucional</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">

                                        <input type="text" id="emailInstitucional" name="emailInstitucional" class="form-control email" placeholder="nombre@institucion.com">

                                        

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="genero_id">Genero</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <select name="genero_id" id="genero_id" class="form-control">
                                        @foreach ($genero as $gen)
                                            <option value="{{$gen->id}}">{{$gen->tipo}}</option>@endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="estadoCivil_id">Estado Civil</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <select name="estadoCivil_id" id="estadoCivil_id" class="form-control">
                                        @foreach ($estadoCivil as $est)
                                            <option value="{{$est->id}}">{{$est->tipo}}</option> @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="profesion_id">Profesion</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <select name="profesion_id" id="profesion_id" class="form-control">
                                        @foreach ($profe as $prof)
                                            <option value="{{$prof->id}}">{{$prof->nombre}}</option>@endforeach
                                    </select>
                                </div>
                            </div>
                        </div>


                        

            <div class="container">
                <div class="header">
                    <h2>
                        Formulario para ingresar la direccion del empleado
                    </h2>
                </div>

                <br>
                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="pais">Asigne el Pais</label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                        <div class="form-group">
                            <select name="pais" id="pais" class="form-control">
                                
                                <option value="">Seleccione un Pais</option>
                                @foreach ($pais as $pais)
                                    <option value="{{$pais->id}}">{{$pais->nombre}}</option>
                                @endforeach
                                </select>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="reg">Asigne la Region</label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                        <div class="form-group">
                            <select name= "region" id="region" class="form-control">
                                <option selected value="" >Seleccione una opcion</option>
                            </select>

                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="subregion_id">Asigne la subregion</label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                        <div class="form-group">
                            <select name= "subregion_id" id="subregion_id" class="form-control">
                                <option selected value="" >Seleccione una opcion</option>

 <!--
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="reg">Asigne la Region</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <select name="reg" id="reg" class="form-control">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="subregion_id">Asigne la subregion</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <select name="subregion_id" id="subregion_id" class="form-control">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="nombre">Direccion</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="detalle" name="detalle" class="form-control" placeholder="calle o avenida">
                                        </div>
                                    </div>
                                </div>
                            </div>  -->

                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="nombre">Direccion</label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="detalle" name="detalle" class="form-control" placeholder="calle o avenida">
                                </div>
                            </div>
                        </div>
                    </div>



                    </div>

                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Guardar</button>
                                <a href="{{route('empleado.index')}}" type="button" class="btn btn-secondary m-t-15 waves-effect">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $( "#main" ).validate({
            rules: {
                codigo:{
                    required: true,
                    maxlength:15
                },
                primerNombre:{
                    required:true,
                    maxlength:50
                },
                segundoNombre:{
                    required:true,
                    maxlength:50
                },
                apellidoPaterno:{
                    required: true,
                    maxlength: 50
                },
                apellidoMaterno: {
                    required:true,
                    maxlength: 50
                },
                apellidoCasada: {
                    maxlength:50
                },
                dui:{
                    required: true,
                    maxlength:10
                },
                nit:{
                    required:true,
                    maxlength:true
                },
                isss:{
                    required:true,
                    maxlength:10
                },
                nup:{
                    required:true,
                    maxlength:12
                },
                pasaporte:{
                    maxlength:10
                },
                emailPersonal:{
                    email:true
                },
                emailInstitucional:{
                    email:true
                },
                genero_id:{
                    required:true
                },
                estadoCivil_id:{
                    required:true
                },
                profesion_id:{
                    required:true
                },
                detalle:{
                    required:true,
                    maxlength:500
                }
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });
        $('#bs_datepicker_container input').datepicker({
            autoclose: true,
            container: '#bs_datepicker_container'
        });

        $('#pais').on('change', function () {
            var regiones = $('#reg');
           $.ajax({
               url: '../get-regiones',
               data:{
                   id:$(this).val()
               }
           }).done(function (data) {
               regiones.empty();
                $.each(data, function (key, item) {
                    regiones.append('<option value="'+ item.id +'">'+ item.nombre +'</option>').selectpicker('refresh');
                });
            });
        });
    </script>
 <!-- aqui estaba el end seccion -->


<script>

 $("#pais").change(function () {

     $.ajax('../SelectPais/' + $('#pais').val()).done(function (data) {
         var regiones = $('#region');
         regiones.empty();
         regiones.append('<option value="" > Seleccione una Region </option>').selectpicker('refresh');
         $.each(data, function (key,item) {
             regiones.append('<option value="'+item.id+'" > '+item.nombre+' </option>').selectpicker('refresh');
         });
     });
 });
 $("#region").change(function () {

     $.ajax('../SelectRegion/' + $('#region').val()).done(function (data) {
         var subregiones = $('#subregion_id');
         subregiones.empty();
         subregiones.append('<option value="" > Seleccione una subRegion </option>').selectpicker('refresh');
         $.each(data, function (key,item) {

             subregiones.append('<option value="'+item.id+'" > '+item.nombre+' </option>').selectpicker('refresh');
         });
     });
 });
</script>

@endsection
