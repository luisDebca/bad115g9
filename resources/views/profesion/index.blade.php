@extends('layouts.appTable')
@section('content')
  <!--  <div class="block-header">
        <h2>
            JQUERY DATATABLES
            <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small>
        </h2>
    </div> -->
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li class="active"><i class="material-icons">archive</i> Profesiones</li>
        </ol>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Profesiones u oficios
                    </h2>
                   
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                              </a>
                             
                       
                                <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('profesion.create')}}"><i class="material-icons">add</i> Nueva</a></li>
                                </ul>
                    
                        </li>
                    </ul>

                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                          
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                
                            
                            </tr>
                            </tfoot>
                            <tbody>
               @if($profesiones->count())
              @foreach($profesiones as $pro)  
              <tr>
                <td>{{$pro->nombre}}</td>
                <td>{{$pro->descripcion}}</td>
                <td> <ul class="list-unstyled">
                                            <li class="dropdown">
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                
                                                    <li><a href="{{ route('profesion.edit', $pro->id) }}"><i class="material-icons">edit</i> Editar</a></li>
                                                    <li><a href="javascript:" class="deleteBtn" data-name="{{ $pro->nombre }}" data-id="{{ $pro->id }}"><i class="material-icons">delete</i> Eliminar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                               



               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="delete-form" method="post" action="javascript:">
                    {{ method_field('delete') }}
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Eliminar profesion u oficio</h4>
                    </div>
                    <div class="modal-body">
                        Se eliminara la profesion <strong id="to-delete-name"></strong>
                        <p>¿Desea continuar?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger waves-effect">Eliminar</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(".deleteBtn").on('click',function () {
            var id = $(this).data('id');
            var name = $(this).data('name');
            $("#to-delete-name").text(name);
            $("#delete-form").attr('action',window.location.href + '/' + id);
            $("#deleteModal").modal("show");
        });
    </script>
@endsection