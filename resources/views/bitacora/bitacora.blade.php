@extends('layouts.appTable')
@section('content')
<!--Contenido -->
<div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li class="active"><i class="material-icons">archive</i>Bitacora</li>
        </ol>
</div>


<!-- Basic Examples -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
          Bitacoras
        </h2>
        @include('mensajes.messages')
        <ul class="header-dropdown m-r--5">
          <li class="dropdown">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">more_vert</i>
            </a>
            <ul class="dropdown-menu pull-right">
              <li><a href="javascript:void(0);">Otra Opcion</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="body">
        <div class="table-responsive">
          <table id="dataTable" class="table table-bordered table-striped table-hover js-basic-example dataTable">
            <thead>
              <tr>
                <th>Id</th>
                <th>Fecha</th>
                <th>Descripcion</th>
                <th>Usuario</th>
                <th># de Planilla</th>
                <th>Fecha Inicio Planilla</th>
                <th>Fecha Final Planilla</th>
              </tr>
            </thead>
            <tbody>
             @foreach ($bitacoras as $b)
             <tr>
               <td>{{ $b->id}}</td>
               <td>{{ $b->fecha }}</td>
               <td>{{ $b->descripcion }}</td>
               <td>{{ $b->name }}</td>            
               <td>{{ $b->numero }}</td>
               <td>{{ $b->fechaInicio }}</td>
               <td>{{ $b->fechaFinal }}</td>
              </td>
            </tr>
            @endforeach 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<!-- #END# Basic Examples -->


@endsection