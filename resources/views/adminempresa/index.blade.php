@extends('layouts.appTable')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li class="active"><i class="material-icons">archive</i> Administración de empresas</li>
        </ol>
    </div>
    @if(session()->get('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Registro de empresas
                    </h2>
                    <small>Empresas registradas en el sistema</small>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{route('ad-empresa.create')}}"><i class="material-icons">add</i> Nuevo</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Logo</th>
                                <th>Nombre</th>
                                <th>Dirección</th>
                                <th>Representante Legal</th>
                                <th>Contactos</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($empresas as $empresa)
                                <tr>
                                    <td><img src="/images/{{$empresa->logoDir}}" alt="N/A" width="100" height="100"></td>
                                    <td>{{$empresa->nombre}}</td>
                                    <td>{{$empresa->direccion}}</td>
                                    <td>{{$empresa->representateLegal}}</td>
                                    <td>
                                        <ul style="list-style: none">
                                            <li>{{$empresa->telefono}}</li>
                                            <li>{{$empresa->email}}</li>
                                            <a  href="../{{$empresa->web}}">{{$empresa->web}}</a>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-unstyled">
                                            <li class="dropdown">
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="{{ route('ad-empresa.show',$empresa->id) }}" class="show-empresa-info" data-id="{{$empresa->id}}"><i class="material-icons">details</i> Detalles</a></li>
                                                    <li><a href="{{route('ad-empresa.edit',$empresa->id)}}"><i class="material-icons">edit</i> Editar</a></li>
                                                    <li><a href="javascript:" class="deleteBtn" data-name="{{ $empresa->nombre }}" data-id="{{ $empresa->id }}"><i class="material-icons">delete</i> Eliminar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="delete-form" method="post" action="javascript:">
                    {{ method_field('delete') }}
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Eliminar Empresa</h4>
                    </div>
                    <div class="modal-body">
                        Se eliminara la empresa <strong id="to-delete-name"></strong>
                        <p>¿Desea continuar?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger waves-effect">Eliminar</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="infoModalLabel">Información</h4>
                </div>
                <div class="modal-body">
                    <img id="modal-img" src="javascript:" width="100%" height="200">
                    <blockquote id="info-name">
                        <p>Nombre</p>
                        <footer class="info-text"></footer>
                    </blockquote>
                    <blockquote id="info-direccion">
                        <p>Dirección</p>
                        <footer class="info-text"></footer>
                    </blockquote>
                    <blockquote id="info-legal">
                        <p>Representante Legal</p>
                        <footer class="info-text"></footer>
                    </blockquote>
                    <blockquote id="info-nit">
                        <p>NIT</p>
                        <footer class="info-text"></footer>
                    </blockquote>
                    <blockquote id="info-nic">
                        <p>NIC</p>
                        <footer class="info-text"></footer>
                    </blockquote>
                    <blockquote id="info-phone">
                        <p>Teléfono</p>
                        <footer class="info-text"></footer>
                    </blockquote>
                    <blockquote id="info-email">
                        <p>Email</p>
                        <footer class="info-text"></footer>
                    </blockquote>
                    <blockquote id="info-web">
                        <p>Sitio Web</p>
                        <footer class="info-text"></footer>
                    </blockquote>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $(document).on('click','.show-empresa-info',function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax(url)
                .done(function (data) {
                    $(".modal-body #info-name .info-text").text(data.nombre);
                    $(".modal-body #info-direccion .info-text").text(data.direccion);
                    $(".modal-body #info-legal .info-text").text(data.representateLegal);
                    $(".modal-body #info-nit .info-text").text(data.nit);
                    $(".modal-body #info-nic .info-text").text(data.nic);
                    $(".modal-body #info-phone .info-text").text(data.telefono);
                    $(".modal-body #info-email .info-text").text(data.email);
                    $(".modal-body #info-web .info-text").text(data.web);
                    $('#modal-img').attr('src','/images/' + data.logoDir);
                });
            $('#infoModal').modal('show');
        });

        $(document).on('click','.deleteBtn',function () {
            var id = $(this).data('id');
            var name = $(this).data('name');
            $("#to-delete-name").text(name);
            $("#delete-form").attr('action',window.location.href + '/' + id);
            $("#deleteModal").modal("show");
        });
    </script>
@endsection
