@extends('layouts.appTable')

@section('content')
    <div class="block-header">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="material-icons">home</i> Inicio</a></li>
            <li class="active"><i class="material-icons">unarchive</i><b>Informacion de la Empresa</b></li>
        </ol>
    </div>   
    @if(session()->get('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                <h2>Información General de la Empresa "<b>{{$empresa->nombre}}</b>"</h2>
            </div>

                <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4">
                                    <div class="card">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                    <img alt="No se ha cargado ninguna Imagen" class="card-img-top embed-responsive-item" src="/images/{{$empresa->logoDir}}"/>
                                                 </div>
                                    </div>                                    
                                    <div class="table-responsive">
                                            <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                                            <th>Email:</th>
                                            <td>{{$empresa->email}}</td>
                                        </tr>
                                        <tr>
                                            <th>Web:</th>
                                            <td>{{$empresa->web}}</td>
                                        </tr>
                                    </table>
                                    </div>
                                </div>
                                
                                <div class="col-sm-8">
                                        <div class="table-responsive">
                                                <table id="dataTable" class="table table-striped table-hover js-basic-example dataTable">
                                                    <tr>
                                                        <th>Nombre:</th>
                                                    <td>{{$empresa->nombre}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Dirección:</th>
                                                        <td>{{$empresa->direccion}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Representante Legal:</th>
                                                        <td>{{$empresa->representateLegal}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>NIT:</th>
                                                        <td>{{$empresa->nit}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>NIC:</th>
                                                        <td>{{$empresa->nic}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Teléfono:</th>
                                                        <td>{{$empresa->telefono}}</td>
                                                    </tr>
                                                    
                                                    </table>
                                            </div>
                                            
                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                <a href="{{ route('empresa.edit', $empresa->id) }}" type="button" class="btn btn-primary m-t-15 waves-effect">Editar</a>
                            </div>                              
                </div>
            </div>
        </div>
    </div>
@endsection