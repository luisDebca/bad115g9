@extends('layouts.applogin')

@section('content')
<div class="card">
  <div class="body">
    {!!Form::model($user,['method'=>'PATCH','route'=>['usuarios.update',$user->id]])!!}
    {{Form::token()}} 
    <div class="msg">Actualizar Usuario: {{$user->name}}</div>
    <div class="span12">
      @include('mensajes.messages')
    </div>
    <div class="input-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <span class="input-group-addon">
        <i class="material-icons">person</i>
      </span>

      <div class="form-line">
        <input id="name" type="text"  class="form-control" name="name" value="{{ $user->name }}" disabled>

        @if ($errors->has('name'))
        <span class="help-block">
         <strong>{{ $errors->first('name') }}</strong>
       </span>
       @endif
     </div>
   </div>
   <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <span class="input-group-addon">
      <i class="material-icons">email</i>
    </span>
    <div class="form-line">
      <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}"
      disabled>

      @if ($errors->has('email'))
      <span class="help-block">
       <strong>{{ $errors->first('email') }}</strong>
     </span>
     @endif
   </div>

 </div>
 <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
  <span class="input-group-addon">
    <i class="material-icons">lock</i>
  </span>
  <div class="form-line">
    <input id="password" type="password" minlength="10" class="form-control" name="password" required placeholder="Contraseña">
    @if ($errors->has('password'))
    <span class="help-block">
     <strong>{{ $errors->first('password') }}</strong>
   </span>
   @endif
 </div>
</div>
<div class="input-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
  <span class="input-group-addon">
    <i class="material-icons">lock</i>
  </span>
  <div class="form-line">
    <input id="password-confirm" type="password" minlength="10"  class="form-control" name="password_confirmation" required placeholder="Confirmar Contraseña">
    @if ($errors->has('password_confirmation'))
    <span class="help-block">
     <strong>{{ $errors->first('password_confirmation') }}</strong>
   </span>
   @endif
 </div>
</div>
<div class="input-group{{ $errors->has('rol_id') ? ' has-error' : '' }}">
  <span class="input-group-addon">
    <i class="material-icons">list</i>
  </span>
  <div class="form-line">                    
    <select name="rol_id" required class="form-control" class="form-control selectpicker"  id="rol_id" value="{{old('rol_id')}}">
                   @foreach ($roles as $r)
                          <option value="{{$r->id}}" @if ($user->rol_id==$r->id)selected="selected" @endif>
                         {{$r->nombre}}</option>
                   @endforeach
                </select>
    @if ($errors->has('rol_id'))
    <span class="help-block">
      <strong>{{ $errors->first('rol_id') }}</strong>
    </span>
    @endif
  </div>
</div>
<div class="input-group{{ $errors->has('empresa_id') ? ' has-error' : '' }}">
  <span class="input-group-addon">
    <i class="material-icons">list</i>
  </span>
  <div class="form-line">                    
    <select name="empresa_id" required class="form-control" class="form-control selectpicker"  id="empresa_id" value="{{old('empresa_id')}}">
                   @foreach ($empresas as $e)
                          <option value="{{$e->id}}" @if ($user->empresa_id==$e->id)selected="selected" @endif>
                         {{$e->nombre}}</option>
                   @endforeach
                </select>
    @if ($errors->has('empresa_id'))
    <span class="help-block">
      <strong>{{ $errors->first('empresa_id') }}</strong>
    </span>
    @endif
  </div>
</div>
<button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">
 <i class="glyphicon glyphicon-refresh"></i> Actualizar
</button>

<button href="{{url('usuarios')}}" class="btn btn-danger btn-block btn-lg bg-pink waves-effect" role="button"><i class="glyphicon glyphicon-remove-circle"></i> Cancelar</button> 

<div class="form-group"> 
  <span>
    @include('mensajes.messages')
  </span>
</div>  
{!!Form::close()!!}
</div>
</div>
@endsection
