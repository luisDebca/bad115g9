<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'Empresas';

    protected $fillable = [
        'nombre',
        'direccion',
        'representateLegal',
        'nit',
        'nic',
        'telefono',
        'email',
        'web',
        'logoDir'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usuarios()
    {
        return $this->hasMany('App\Usuario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function unidadesOrganizacionales()
    {
        return $this->hasMany('App\UnidadOrganizacional');
    }
}
