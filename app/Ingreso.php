<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    protected $table = 'Ingresos';

    protected $fillable = ['nombre','taza','tipo','montoFijo'];
}
