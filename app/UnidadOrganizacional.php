<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class UnidadOrganizacional extends Model
{
    protected $table = 'UnidadesOrganizacionales';

    protected $fillable = ['codigo','nombre','descripcion','empresa_id','unidadPadre_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function centrosCostos()
    {
        return $this->hasMany('App\CentroCosto');
    }

    /**
     * @return Empresa
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contratos()
    {
        return $this->hasMany('App\ContratoTrabajo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subUnidades()
    {
        return $this->hasMany('App\UnidadOrganizacional');
    }

    /**
     * @return UnidadOrganizacional
     */
    public function unidadPrincipal()
    {
        return $this->belongsTo('App\UnidadOrganizacional');
    }

}
