<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Planilla extends Model
{
    protected $table = 'Planillas';

    protected $fillable = ['numero','fechaInicio','fechaFinal'];

    /**
     * @return ContratoTrabajo
     */
    public function contratoTrabajo()
    {
        return $this->belongsTo('App\ContratoTrabajo');
    }
}
