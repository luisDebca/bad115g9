<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class ContratoTrabajo extends Model
{
    protected $table = 'ContratosTrabajo';

    protected $fillable = [
        'fechaInicio',
        'fechaFin',
        'salario',
        'habilitado'
    ];

    /**
     * @return UnidadOrganizacional
     */
    public function unidadOrganizacional()
    {
        return $this->belongsTo('App\UnidadOrganizacional');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function planillas()
    {
        return $this->hasMany('App\Planilla');
    }

    /**
     * @return PuestoTrabajo
     */
    public function puestoTrabajo()
    {
        return $this->belongsTo('App\PuestoTrabajo');
    }

    /**
     * @return Empleado
     */
    public function empleado()
    {
        return $this->belongsTo('App\Empleado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoContrato()
    {
        return $this->belongsTo('App\TipoContrato');
    }
}
