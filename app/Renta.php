<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Renta extends Model
{
    protected $table = 'Rentas';

    protected $fillable = [
        'tramo',
        'desde',
        'hasta',
        'porcentaje',
        'sobreExceso',
        'cuotaFija',
        'retencion'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function descuentos()
    {
        return $this->hasMany('App\Descuento');
    }
}
