<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
	protected $table = 'Generos';
	
    protected $fillable = ['tipo'];

    public function empleados(){
        return $this->hasMany(Empleado::class);
    }
}
