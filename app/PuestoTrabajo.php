<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class PuestoTrabajo extends Model
{
    protected $table = 'PuestosTrabajo';

    protected $fillable = ['nombre','descripcion','salarioMaximo','salarioMinimo'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contratos()
    {
        return $this->hasMany('App\ContratoTrabajo');
    }
}
