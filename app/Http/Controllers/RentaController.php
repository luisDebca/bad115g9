<?php

namespace bab115g9\Http\Controllers;

use bab115g9\Renta;
use Illuminate\Http\Request;

class RentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rentas = Renta::all();
        return view('renta.index', compact('rentas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('renta.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRenta($request);
        $renta = new Renta([
            'tramo' => $request->get('tramo'),
            'desde' => $request->get('desde'),
            'hasta' => $request->get('hasta'),
            'porcentaje' => $request->exists('porcentaje') ? $request->get('porcentaje'): 0.00,
            'sobreExceso' => $request->exists('sobreExceso') ? $request->get('sobreExceso'): 0.00,
            'cuotaFija' => $request->get('cuotaFija') ? $request->get('cuotaFija'): 0.00,
            'retencion' => $request->get('retencion')
            ]);
        $renta->save();
        return redirect('/rentas')->with('message','Nueva Renta guardada correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $renta = Renta::find($id);
        return view('renta.show',compact('renta'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $renta = Renta::find($id);
        return view('renta.edit', compact('renta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateRenta($request);
        $renta = Renta::find($id);
        $renta->tramo = $request->get('tramo');
        $renta->desde = $request->get('desde');
        $renta->hasta = $request->get('hasta');
        $renta->porcentaje = $request->exists('porcentaje') ? $request->get('porcentaje'): 0.00;
        $renta->cuotaFija = $request->exists('sobreExceso') ? $request->get('sobreExceso'): 0.00;
        $renta->retencion = $request->get('retencion');
        $renta->save();

        return redirect('/rentas')->with('message','Renta editada correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $renta = Renta::find($id);
        $renta->delete();

        return redirect('/rentas')->with('message','Renta eliminada correctamente.');
    }

    private function validateRenta($request)
    {
        $request->validate([
           'tramo'=>'required',
           'desde'=>'required',
           'hasta'=>'required',
           'retencion'=>'required'
        ]);
    }
}
