<?php

namespace bab115g9\Http\Controllers;

//use Illuminate\Http\Request;

use bab115g9\UnidadOrganizacional;

use bab115g9\CentroCosto;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Request;
use Carbon\Carbon;
use DateTime;
use DB;

class CentroCostosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costos=CentroCosto::all();
        $unidades = UnidadOrganizacional::where('tipo', '=', 'Departamento')->get();
        return view('costos.index', compact(['costos','unidades']));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $now = Carbon::now();


        
        $count = DB::table('CentroCostos')
                ->whereYear('created_at',$now->year )
                ->count();
                if  ($count==0){
                    $unidades = UnidadOrganizacional::where('tipo', '=', 'Departamento')->get();
        return view('costos.create', compact(['unidades']));

                }
                else{
                    return back()->with('message','La Asignacion de los costos por departam,entos ya fue asignada');

                }

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Request::all();
        $rules = [];
        $now = Carbon::now();
        foreach($input['monto'] as $indexKey => $val)
        {
            $rules['monto.'.$indexKey] = 'required';
        }
        
        $validator = Validator::make($input, $rules);
        //Verificar Validación
        if ($validator->fails())
        {
            return redirect('/centro-costos/create')->with('status','Los campos no deben estar Vacios');
        }
        $input = Input::all();
        $now = new DateTime(); 
        $condition = $input['monto'];
        foreach ($condition as $indexKey => $condition) {            
            
        $dep=substr($input['departamento'][$indexKey], 0, 3); 
        $costo = new CentroCosto;
        //$costo->numero = $input['numero'][$indexKey];
        $costo->numero = 'C'.$dep;
        $costo->monto = $input['monto'][$indexKey];
        $costo->periocidad ='Anual';
        //$costo->anioCalendario = Carbon::now();
        $costo->anioCalendario = $now;               
        $costo->unidad_id = $input['unidad_id'][$indexKey];
        $costo->save();
    }
    
    //return redirect('/centro-costos/index')->with('status','Los Registros han sido guardados exitosamente');
    return redirect()->action('CentroCostosController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function remove()
    {
        //$now = Carbon::now();
        //$nowYear =$now->year;
        //DB::table('CentroCostos')->whereYear('anioCalendario',"=",$nowYear)->delete();
        
    //return redirect()->action('CentroCostosController@index');


    }
}
