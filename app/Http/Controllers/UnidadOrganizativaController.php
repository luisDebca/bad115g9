<?php

namespace bab115g9\Http\Controllers;

//use Illuminate\Http\Request;
use DB;
use Request;
use bab115g9\UnidadOrganizacional;
use bab115g9\Empresa;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Auth;
use bab115g9\Http\Requests\UnidadRequest;

class UnidadOrganizativaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();       
        $count  = UnidadOrganizacional::where('tipo', '=', 'Departamento')->count();
        if ($count == 0){

            return view('unidades.create')->with('message','No se ha Registrado Ningun Departamento, Agrega Nuevos Registros');

        }   else {
            
    $unidades = UnidadOrganizacional::where('tipo', '=', 'Departamento')->get(); 
    //$paramcod='ZZZ';       
    //$resp = DB::select ("select * from getlastunitid(?)", array($paramcod))[0]->getlastunitid == null? 'es nulo': 'ok';
    //$resp = DB::select ("select * from getlastunitid(?)", array($paramcod))[0]->getlastunitid;



            return view('unidades.index', compact(['unidades','user']));

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $count = Empresa::count();
        if ($count == 0){
            return redirect('/empresa/create')->with('advice',
            'Aun no se ha Registrado Ninguna Empresa, Para Agregar 
            Unidad debe Ingresar datos de Empresa');
        }
        else{
            return view('unidades.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $depto = UnidadOrganizacional::find($id);
        $tipo = $depto->tipo;
        $id=$id;

        //$unidades = DB::table('UnidadesOrganizacionales')->select('tipo',$tipo )->get();
        $count = DB::table('UnidadesOrganizacionales')->where('unidadPadre_id', $id)->count();        
        if ($count == 0){ 
        
        return view('unidades.show',compact('depto','count','id'));
        }
        else{
           // $unidades = DB::table('UnidadesOrganizacionales')->where('tipo', $tipo)->count();
            //return view('unidades.show',compact('depto','unidades'));
            $unidades = UnidadOrganizacional::where('unidadPadre_id', '=', $id)->get();
        return view('unidades.show',compact('depto','count','unidades', 'id'));
            }
        //$depto = UnidadOrganizacional::find($id);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

function insert(Request $request)
    {
        
        $input = Request::all();
        $rules = [];
        foreach($input['nombre'] as $key => $val)
        {
            $rules['nombre.'.$key] = 'required';
        }
        
        $validator = Validator::make($input, $rules);
        //Verificar Validación
        if ($validator->fails())
        {
            return redirect('/unidad-organizativa/create')->with('status','Los campos para subunidad no deben estar Vacios');
        }
        
        $empresa = DB::table('Empresas')->latest()->first();
        $tipo = 'Departamento';
        $tipo1 = 'Area';
        $input = Input::all();
        $condition = $input['nombre'];
        foreach ($condition as $key => $condition) {
        $namp1=substr($tipo, 0, 1);
        $namp2=substr($tipo1, 0, 1);        
        $namp3 = substr($input['nombre'][$key], 0, 1);
        $namp4='001';

        $unidad = new UnidadOrganizacional;

        $unidad->codigo = $namp1.$namp2.$namp3.$namp4;
        $unidad->nombre = $input['nombre'][$key];
        $unidad->tipo = $tipo1;        
        $unidad->descripcion = $input['descripcion'][$key];
        $unidad->empresa_id = $empresa->id;
        $unidad->unidadPadre_id = $input['unidadPadre_id'][$key];
        $unidad->save();
    }
    return back()->with('status','Los Registros han sido guardados exitosamente');   
    //return redirect('/unidad-organizativa/index')->with('status','Los Registros han sido guardados exitosamente');
    
    }
    
public function save(Request $request)
    {
        $input = Request::all();
        $rules = [];
        foreach($input['nombre'] as $key => $val)
        {
            $rules['nombre.'.$key] = 'required';
        }
        
        $validator = Validator::make($input, $rules);
        //Verificar Validación
        if ($validator->fails())
        {
            return redirect('/unidad-organizativa/create')->with('status','Los campos no deben estar Vacios');
        }
        
        //$empresa = DB::table('Empresas')->latest()->first();
        $user = auth()->user();
        $tipo = 'Departamento';
        $input = Input::all();
        $condition = $input['nombre'];
        foreach ($condition as $key => $condition) {

        $cod=substr($input['nombre'][$key], 0, 3);
        //$paramcod=substr($input['nombre'][$key], 0, 3);

        $resp = DB::select ("select * from getlastunitid(?)", array($cod))[0]->getlastunitid;

        if ($resp === null) {
            $invID3 = str_pad(1, 4, '0', STR_PAD_LEFT);
}
else{  
    $newcode = (int)substr($resp, 5, 8);
    $newaddcode = $newcode + 1;   
    $invID3 = str_pad($newaddcode, 4, '0', STR_PAD_LEFT);

}

/*$contador1=0;
            $invID1 = str_pad($contador1, 4, '0', STR_PAD_LEFT);
            $contador2 = 1;
            $invID2 = str_pad($contador2, 4, '0', STR_PAD_LEFT);
            $total= $invID1+$invID2;
            $invID3 = str_pad($total, 4, '0', STR_PAD_LEFT);*/

        $unidad = new UnidadOrganizacional;
        $unidad->codigo=$cod.$invID3;
        $unidad->nombre = $input['nombre'][$key];
        $unidad->tipo = $tipo;
        $unidad->descripcion = $input['descripcion'][$key];
        $unidad->empresa_id = $user->empresa_id;
        $unidad->save();
        
    }
    
    //return redirect('/unidad-organizativa/index')->with('status','Los Registros han sido guardados exitosamente');
    return redirect()->action('UnidadOrganizativaController@index')->with('status','Los Registros han sido guardados exitosamente');
}
                
public function validarForm(Request $request)
{
    $request->validate(
        [
        'codigo'=>'required',
        'nombre' => 'required|max:100',
        'descripcion'=>'required|max:500',

        ]);
    
}  

}
