<?php

namespace bab115g9\Http\Controllers;

use bab115g9\PuestoTrabajo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PuestosResumenController extends Controller
{

    public function nombres()
    {
        $puestos = DB::select('select * from getnombrepuestos()');
        return $puestos;
    }

    public function rangoSalario(Request $request)
    {
        $id = $request->get('id');
        $rango = DB::select('select * from getrangosalario(?)',array($id));
        return $rango;
    }

    public function cuenta()
    {
        $cuenta = DB::select('select * from rowcount_all() where table_name = ?',array('PuestosTrabajo'));
        return $cuenta;
    }
}
