<?php

namespace bab115g9\Http\Controllers;

use bab115g9\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{

    public function welcome()
    {
        $user = auth()->user();
        if ($user->rol_id == 1){
            return view('home');
        }else {
            $infoEmpresa = Empresa::find($user->empresa_id);
            $departamentos = DB::select("select * from getdepartamentosresumen(?)",array($user->empresa_id));
            return view('welcome',[
                'info' => $infoEmpresa,
                'departamentos' => $departamentos
            ]);
        }
    }
}
