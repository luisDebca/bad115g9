<?php

namespace bab115g9\Http\Controllers;

use bab115g9\Comision;
use Illuminate\Http\Request;

class ComisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comisiones = Comision::all();
        return view('comision.index', compact('comisiones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('comision.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateComision($request);
        $request->validate(['nombre'=>'unique:Comisiones']);
        $comision = new Comision([
           'nombre' => $request->get('nombre'),
           'valorMenor' => $request->get('valorMenor'),
           'valorMayor' => $request->get('valorMayor'),
           'taza' => $request->get('taza')
        ]);
        $comision->save();
        return redirect('/comisiones')->with('message', 'La comisión fue guardada correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comision = Comision::find($id);
        return $comision;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comision = Comision::find($id);
        return view('comision.edit', compact('comision'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateComision($request);
        $comision = Comision::find($id);
        $comision->nombre = $request->get('nombre');
        $comision->valorMenor = $request->get('valorMenor');
        $comision->valorMayor = $request->get('valorMayor');
        $comision->taza = $request->get('taza');
        $comision->save();
        return redirect('/comisiones')->with('message','Comisión actualizada correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comision = Comision::find($id);
        $comision->delete();
        return redirect('comisiones/')->with('message', 'Comisión eliminada correctamente.');
    }

    public function validateComision($request)
    {
        $request->validate([
           'nombre'=>'required|max:50',
           'valorMenor'=>'required|regex:/^\d+(\.\d{1,2})?$/',
           'valorMayor'=>'required|regex:/^\d+(\.\d{1,2})?$/',
           'taza'=>'required|regex:/^\d+(\.\d{1,2})?$/'
        ]);
    }
}
