<?php

namespace bab115g9\Http\Controllers\Auth;

use bab115g9\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use bab115g9\Http\Requests;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    

    public $maxAttempts=3;

    //public $decayMinutes=30;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    
    protected function credentials(Request $request)
    {
    $credentials = $request->only($this->username(), 'password');
    //añadimos el valor de activo a S, para que sea correcto
    $credentials  = array_add($credentials, 'habilitado', '1');
    Session::flash('block','Su usuario esta deshabilitado porfavor comuniquese con Departamento de Informatica!!!');
    return $credentials  ;
    }
}
