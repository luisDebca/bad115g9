<?php

namespace bab115g9\Http\Controllers;

use bab115g9\Comision;
use bab115g9\Descuento;
use bab115g9\Empresa;
use bab115g9\Ingreso;
use bab115g9\Planilla;
use bab115g9\UnidadOrganizacional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class PlanillaProcessController extends Controller
{

    public function unidades(Request $request)
    {
        $departamentos = array();
        for ($index = 0; $index < sizeof($request->get('departamentos')); $index++){
            $departamentos[$index] = UnidadOrganizacional::find($request->get('departamentos')[$index]);
        }
        return view('planilla.seleccionarEmpleados',['departamentos' => $departamentos]);
    }

    public function nuevaPlanilla(Request $request)
    {
        $user = auth()->user();
        $maxNumber = DB::select('select * from getmaxnumeroplanilla(?)',array($user->empresa_id))[0]->getmaxnumeroplanilla;
        foreach ($request->empleados as $emp){
            $contrato = DB::select('select * from getcontratoactual(?)',array($emp))[0]->getcontratoactual;
            if ($contrato != null){
                $numeroPlanilla = strval($maxNumber + 1);
                DB::select('select * from ingresarplanilla(?,?,?)',array($numeroPlanilla,$contrato,$user->empresa_id));
            }
        }
        return redirect('/planillas')->with('message','Nueva planilla creada correctamente.');
    }

    public function mostrarEmpleados($numeroPlanilla)
    {
        $totalSalario = 0.0;
        $totalIngresos = 0.0;
        $totalDescuentos = 0.0;
        $totalComisiones = 0.0;
        $empleados = DB::select('select * from getempleadosplanilla(?)',array($numeroPlanilla));
        foreach ($empleados as $emp){
            $totalSalario += $emp->salario;
            $totalIngresos += $emp->ingresos;
            $totalDescuentos += $emp->descuentos;
            $totalComisiones += $emp->comisiones;
        }
        return view('planilla.modificarEmpleados',
            [
                'empleados' => $empleados,
                'totalSalario' => $totalSalario,
                'totalIngresos' => $totalIngresos,
                'totalDescuentos' => $totalDescuentos,
                'totalComisiones' => $totalComisiones
            ]);
    }

    public function agregarIngresoPlanilla($planilla)
    {
        $numero = Planilla::find($planilla)->numero;
        $ingresos = Ingreso::all();
        $salario = DB::select('select * from getsalario(?)',array($planilla))[0]->getsalario;
        return view('planilla.ingresoPlanilla', ['ingresos' => $ingresos,'salario' => $salario, 'numero' => $numero, 'planilla' => $planilla]);
    }

    public function guardarIngresoPlanilla(Request $request, $planilla)
    {
        $usuario = auth()->user();
        $numero = Planilla::find($planilla)->numero;
        DB::select('select * from crearingresoplanilla(?,?,?,?,?,?)',array(
            $request->planilla,
            $request->tipo,
            $request->monto,
            $request->desde,
            $request->hasta,
            $request->estado,
        ));
        return redirect('/planilla-modificar-empleados/' . $numero);
    }

    public function agregarDescuentoPlanilla($planilla)
    {
        $numero = Planilla::find($planilla)->numero;
        $descuentos = Descuento::all();
        $salario = DB::select('select * from getsalario(?)',array($planilla))[0]->getsalario;
        return view('planilla.descuentoPlanilla', ['descuentos' => $descuentos, 'salario' => $salario, 'planilla' => $planilla, 'numero' => $numero]);
    }

    public function guardarDescuentoPlanilla(Request $request, $planilla)
    {
        $numero = Planilla::find($planilla)->numero;
        DB::select('select * from creardescuentoplanilla(?,?,?,?,?,?)',array(
            $request->planilla,
            $request->tipo,
            $request->monto,
            $request->desde,
            $request->hasta,
            $request->estado
        ));
        return redirect('/planilla-modificar-empleados/' . $numero);
    }

    public function agregarComisionPlanilla($planilla)
    {
        $numero = Planilla::find($planilla)->numero;
        $comisiones = Comision::all();
        $salario = DB::select('select * from getsalario(?)',array($planilla))[0]->getsalario;
        return view('planilla.comisionPlanilla', ['comisiones' => $comisiones, 'salario' => $salario, 'planilla' => $planilla, 'numero' => $numero]);
    }

    public function guardarComisionPlanilla(Request $request, $planilla)
    {
        $numero = Planilla::find($planilla)->numero;
        DB::select('select * from crearcomisionplanilla(?,?,?,?,?,?)',array(
            $request->planilla,
            $request->tipo,
            $request->monto,
            $request->desde,
            $request->hasta,
            $request->estado
        ));
        return redirect('/planilla-modificar-empleados/' . $numero);
    }

    public function validarPlanilla($planillaNumero)
    {

    }

    public function editarPlanillas($numero)
    {
        $planilla = Planilla::all()->where('numero','=',$numero)->first();
        return view('planilla.editar', compact('planilla'));
    }

    public function actualizarPlanillas(Request $request)
    {
        $planillas = Planilla::all()->where('numero','=',$request->get('numero'));
        foreach ($planillas as $item)
        {
            $item->fechaInicio = $request->get('fechaInicio');
            $item->fechaFinal = $request->get('fechaFinal');
            $item->save();
        }
        return redirect('/planillas')->with('message','Planilla modificada correctamente.');
    }

    public function getCentrosAfectados(Request $request)
    {
        $costos = DB::select('select * from getplanillacentrocosto(?)',array($request->get('numero')));
        return $costos;
    }

    public function eliminarPlanillaIngreso($planilla, $ingreso)
    {
        $numero = Planilla::find($planilla)->numero;
        DB::select('select * from eliminaringresoplanilla(?,?)', array($planilla, $ingreso));
        return redirect('/planilla-modificar-empleados/'.$numero)->with('message','Ingreso removido de planilla.');
    }

    public function eliminarPlanillaDescuento($planilla, $descuento)
    {
        $numero = Planilla::find($planilla)->numero;
        DB::select('select * from eliminardescuentoplanilla(?,?)', array($planilla, $descuento));
        return redirect('/planilla-modificar-empleados/'.$numero)->with('message','Descuento removido de planilla.');
    }

    public function eliminarPlanillaComision($planilla, $comision)
    {
        $numero = Planilla::find($planilla)->numero;
        DB::select('select * from eliminarcomisionplanilla(?,?)', array($planilla, $comision));
        return redirect('/planilla-modificar-empleados/'.$numero)->with('message','Comisión removida de planilla.');
    }

    public function planillaPdf($numeroPlanilla)
    {
        $totalSalario = 0.0;
        $totalIngresos = 0.0;
        $totalDescuentos = 0.0;
        $totalComisiones = 0.0;
        $usuario = auth()->user();
        $empresa = Empresa::find($usuario->empresa_id);
        $empleados = DB::select('select * from getempleadosplanilla(?)',array($numeroPlanilla));
        foreach ($empleados as $emp){
            $totalSalario += $emp->salario;
            $totalIngresos += $emp->ingresos;
            $totalDescuentos += $emp->descuentos;
            $totalComisiones += $emp->comisiones;
        }

        if(count($empleados)===0){
        Session::flash('query','No hay Resultados para su consulta');
        }
        else{
        $pdf=PDF::loadView('planilla.planillaPdf',[
            'empleados'=>$empleados,
            'empresa' => $empresa,
            'totalSalario' => $totalSalario,
            'totalIngresos' => $totalIngresos,
            'totalDescuentos' => $totalDescuentos,
            'totalComisiones' => $totalComisiones
        ]);
        $papel_tamaño=array(0,0,279,216);
        $pdf->setPaper("letter",'Landscape');
        return $pdf->stream('Planilla');
        }
    }
}