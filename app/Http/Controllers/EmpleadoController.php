<?php

namespace bab115g9\Http\Controllers;

use Illuminate\Http\Request;

use bab115g9\EstadoCivil;
use bab115g9\Genero;
use bab115g9\Profesion;
use bab115g9\Empresa;
use bab115g9\Estado;

use bab115g9\Subregion;
use bab115g9\Region;
use bab115g9\Empleado;
use bab115g9\Pais;

//use Illuminate\Support\Facades\DB;
use DB;


class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $usuario = auth()->user();
        $empleados = DB::select('select * from getresumenempleados(?)',array($usuario->empresa_id));
        return view('empleado.index', compact('empleados'));

        
   
         
        // }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuario = auth()->user();
        $genero = Genero:: all()->where('empresa','=',$usuario->empresa_id);
        $estadoCivil = EstadoCivil:: all()->where('empresa','=',$usuario->empresa_id);
        $profe = Profesion:: all()->where('empresa','=',$usuario->empresa_id);

        $pais = Pais::all()->where('empresa','=',$usuario->empresa_id);;
       // $subre= Subregion::all()->where('empresa','=',$usuario->empresa_id);;
       // $region= Region::all()->where('empresa','=',$usuario->empresa_id);
        return view("empleado.create", compact('genero','profe','estadoCivil','pais'));
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $request->validate([ 'codigo'=> 'required|unique:Empleados|max:15','primerNombre'=> 'required|alpha','segundoNombre'=> 'required|alpha','apellidoPaterno'=>'required|alpha','apellidoMaterno'=>'required|alpha','fechaNacimiento'=>'required','dui'=>'required|unique:Empleados','nit'=>'required|unique:Empleados','isss'=>'numeric|required|unique:Empleados','nup'=>'numeric|required|unique:Empleados','pasaporte'=>'required|unique:Empleados|max:8','emailPersonal'=>'required','emailInstitucional'=>'required','genero_id'=> 'required', 'estadoCivil_id' => 'required', 'profesion_id'=>'required']);

        $emp = new Empleado(['codigo'=>$request->get('codigo'),
            'primerNombre'=>$request->get('primerNombre'),
            'segundoNombre'=>$request->get('segundoNombre'),
            'apellidoPaterno'=>$request->get('apellidoPaterno'),
            'apellidoMaterno'=>$request->get('apellidoMaterno'),
            'apellidoCasada'=>$request->get('apellidoCasada'),
            'fechaNacimiento'=>$request->get('fechaNacimiento'),
            'dui'=>$request->get('dui'),
            'nit'=>$request->get('nit'),
            'isss'=>$request->get('isss'),
            'nup'=>$request->get('nup'),
            'pasaporte'=>$request->get('pasaporte'),
            'emailPersonal'=>$request->get('emailPersonal'),
            'emailInstitucional'=>$request->get('emailInstitucional'),
            'genero_id'=>$request->get('genero_id'),
            'estadoCivil_id'=>$request->get('estadoCivil_id'),
            'profesion_id'=>$request->get('profesion_id'),
            'jefe_id'=>$request->get('jefe_id')
        ]);
        $emp->save();


        
       
       

        $est = new Estado;
        //$consulta->nombreConsulta=$request->get('nombreConsulta');
            $est->detalle=$request->get('detalle');
            $est->subregion_id=$request->get('subregion_id');

        $est->empleado_id=$emp->id;
        $est->save();

        return redirect('/empleado')->with('message','Empleado nuevo guardado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
         $empleado=Empleado::find($id);
         
         $genem = Genero::find($empleado->genero_id);
         $estc = EstadoCivil::find($empleado->estadoCivil_id);
         $prof = Profesion::find($empleado->profesion_id);
         $jef=Empleado::find($empleado->jefe_id);
        return  view('empleado.show',compact('empleado','genem','estc','prof','jef'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $empleado=Empleado::find($id);
         $gen = Genero:: all();
         $estadoCivil = EstadoCivil:: all();
        $profe = Profesion:: all();
        $jefe = Empleado:: all();
          //$gen = Genero::find($empleado->genero_id);
        return view('empleado.edit',compact('empleado','gen','estadoCivil','profe','jefe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 
                
                'codigo'=>'required']);
 
        Empleado::find($id)->update($request->all());
        return redirect()->route('empleado.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Empleado::find($id)->delete();
        return redirect()->route('empleado.index')->with('success','Registro eliminado satisfactoriamente');
    }

    public function byPais($id){
        return Region::where('pais_id', $id)->get();
    }

    public function showChiefsEmployees(Request $request, $id){
        if($request){

            //Datos para la lista de UO
            $chiefsEmployees=DB::select('select * from showchiefs('.$id.');');
            $profesiones = Profesion::all();
            $empresa = Empresa::find($id);
        }

        return view('empleado.showChiefsEmployees',["chiefs"=>$chiefsEmployees,"nombre"=>$empresa], compact( 'profesiones'));
    }

    public function getAllRegiones(Request $request, $id){
        if($request){
            $regiones=DB::select('select * from getRegiones('.$id.');');
        }
        return $regiones;
    }

    public function getAllSubRegiones(Request $request, $id){
        if($request){
            $subregiones=DB::select('select * from getsubregiones('.$id.');');
        }
        return $subregiones;
    }
}
