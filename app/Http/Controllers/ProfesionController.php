<?php

namespace bab115g9\Http\Controllers;

use Illuminate\Http\Request;
use bab115g9\Profesion;

class ProfesionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request){
            
            $profesiones = Profesion::orderBy('id','DESC')->paginate(3);
            return view('profesion.index', compact('profesiones'));
         
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('profesion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(['nombre'=>'required', 'descripcion'=>'required']);
           
        
        $pro = new Profesion(['nombre'=>$request->get('nombre'), 'descripcion'=>$request->get('descripcion')]);
        $pro->save();

        return redirect('/profesion')->with('message','Nueva Profesion guardada correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $pro=Profesion::find($id);
      
       return  view('profesion.show', compact('pro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $pro=Profesion::find($id);
        return view('profesion.edit',compact('pro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 
                
                'nombre'=>'required', 
                'descripcion' =>'required']);
 
        profesion::find($id)->update($request->all());
        return redirect()->route('profesion.index')->with('success','Registro actualizado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Profesion::find($id)->delete();
        return redirect()->route('profesion.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
