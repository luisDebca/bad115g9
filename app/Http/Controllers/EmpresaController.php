<?php

namespace bab115g9\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;
use bab115g9\Empresa;
use DB;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $logo_path;
    private $save_name;
    
    private $imagen;

    public function __construct()
    {
        $this->logo_path = public_path('/images');
    }


    public function index()
    {
        $usuario = auth()->user();
        $empresa = Empresa::find($usuario->empresa_id);
        return view('empresa.index', compact('empresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresa.index2');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarForm($request);
        ini_set('memory_limit','256M');
        if ($request->exists('file')) {
            $logo = $request->file('file');
        
        if (!is_dir($this->logo_path)) {
            mkdir($this->logo_path, 0777);
        }

            $name = sha1(date('YmdHis') . str_random(30));
            $save_name = $name . '.' . $logo->getClientOriginalExtension();
            $resize_name = $name . str_random(2) . '.' . $logo->getClientOriginalExtension();

            Image::make($logo)
                ->resize(250, null, function ($constraints) {
                    $constraints->aspectRatio();
                })
                ->save($this->logo_path . '/' . $resize_name);

            $logo->move($this->logo_path, $save_name);
        }
        else{
            $save_name = null;

        }

        $empresa = new Empresa([
           'nombre' => $request->get('nombre'),
           'direccion' => $request->get('direccion'),           
           'representateLegal' => $request->get('representanteLegal'),
           'nit' => $request->get('nit'),
           'nic' => $request->get('nic'),
           'telefono' => $request->get('telefono'),
           'email' => $request->get('email'),
           'web' => $request->get('web'),
           'logoDir'=>$save_name,          
        ]);
           
                
        $empresa->save();
        return redirect('/empresa')->with('message','Datos de Empresa Guardados Exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('empresa.create')->with('status','No se ha encontrado registro de empresa');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp = Empresa::find($id);
        return view('empresa.edit',compact('emp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validarForm($request);
        //$this->validate($request, ['file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
        ini_set('memory_limit','256M');

          
        if ($request->exists('file')) {
            $logo = $request->file('file');
        
        if (!is_dir($this->logo_path)) {
            mkdir($this->logo_path, 0777);
        }

            $name = sha1(date('YmdHis') . str_random(30));
            $save_name = $name . '.' . $logo->getClientOriginalExtension();
            $resize_name = $name . str_random(2) . '.' . $logo->getClientOriginalExtension();

            Image::make($logo)
                ->resize(250, null, function ($constraints) {
                    $constraints->aspectRatio();
                })
                ->save($this->logo_path . '/' . $resize_name);

            $logo->move($this->logo_path, $save_name);
        }
        else{
            $empresa = Empresa::find($id);
            $save_name = $empresa->logoDir;

        }
        $telefono=$request->telefono;
        $telp1=substr($telefono, 0, 4);
        $telp2=substr($telefono, 5, 4);
        $empresa = Empresa::find($id);
        $empresa->nombre = $request->get('nombre');
        $empresa->direccion = $request->get('direccion');
        $empresa->representateLegal = $request->get('representanteLegal');
        $empresa->nit = $request->get('nit');
        $empresa->nic = $request->get('nic');
        //$empresa->telefono = $request->get('telefono');
        $empresa->telefono = $telp1.$telp2;
        $empresa->email = $request->get('email');        
        $empresa->web = $request->get('web');                
        $empresa->logoDir = $save_name;
        $empresa->save();
        return redirect('/empresa')->with('message','Registros Actualizados Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        

    }

    public function validarForm(Request $request)
    {
        $request->validate(
            [
            'nombre' => 'required|max:100',
            'direccion'=>'required|max:500',
            'nit'=>'required|max:17',
            'nic'=>'required|max:10',
             /*'telefono'=>array(
                'nullable',
                'regex:/(2)[0-8]{8}/',
                'numeric'
                
            ),*/
            //'telefono' => 'nullable|regex:/(2)[0-9]{7}/|numeric',
            'email' => 'email|max:255',
            //'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
            ],
            
            [
            'nombre.required'=>'El Campo Nombre es obligatorio',
            'direccion.required'=>'El campo Direccion es obligatorio',
            'direccion.max'=>'La Dirección no debe superar los 500 caracteres',  
            'email.email'=>'Formato de correo no valido',
            'file.mimes'=>'El archivo debe contener un formato de Imagen valido Eje: .jepg, .png, .gif, .svg, .jpg',
            'file.max'=>'El archivo de Imagen no debe superar de 1024Mb',
            'telefono.regex'=>'Número de telefono no válido',
            'nit.max'=>'El campo NIT no debe superar los 17 caracteres',
            'nic.max'=>'El campo NIC no debe superar los 10 caracteres',
            ]
        );
        
    }
}
