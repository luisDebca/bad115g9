<?php

namespace bab115g9\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PlanillaReportController extends Controller
{

    public function testReport()
    {
        $data = ['title' => 'welcome to Planilla'];
        $pdf = PDF::loadView('report.testPDF', $data);
        return $pdf->stream('test.pdf');
    }

}