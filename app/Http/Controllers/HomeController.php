<?php

namespace bab115g9\Http\Controllers;

use Illuminate\Http\Request;

use bab115g9\Http\Requests;

use Illuminate\Support\Facades\Redirect;

use Carbon\Carbon;
use Session;
use DB;
//para modelos
use bab115g9\User;

use bab115g9\Rol;

use bab115g9\Empresa;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
// Validación de formularios.
use Validator;

// Hash de contraseñas.
use Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
      public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request){
        
        if ($request)
        {
            //$users=User::all();

            //Datos para la mostrar el mantenimiento
            $users=DB::table('users as u')->Select( DB::raw("u.id, u.name, u.email, u.habilitado, u.rol_id, u.ultimo_login, r.nombre, r.descripcion"))
            ->join('Roles as r','u.rol_id','=','r.id')
            ->get();
          // fin
            return view('usuarios.index',["users"=>$users]);
        }
    }
 
    public function create()
    {   
        //dd("hola");
        $roles=Rol::all();
        $empresas=Empresa::all();
        return view("usuarios.create",["roles"=>$roles,"empresas"=>$empresas]);
    }

    public function store(Request $request)
    {
        // Realizamos la validación de datos recibidos del formulario.
        $rules=array(
            'name'=>'required|unique:users', // Username es único en la tabla users
            'email'=>'required|email|unique:users', // Username es único en la tabla users
            'password'=>'required|min:10',
            'password_confirmation'=>'required|same:password',
            'rol_id'=>'required|not_in:0',
            'empresa_id'=>'required|not_in:0'
            );

        $messages= array ('name.required' =>'El Nombre es requerido',
                'email.required'=>'Correo Electronico es requerido',
                'password.required'=>'Contraseña es requerido',
                'password_confirmation.required'=>'Confirmar Contraseña es requerido',
                'rol_id.required'=>'Tipo de Usuario es requerido',
                'empresa_id.required'=>'La empresa es requerido',
                'name.unique' =>'El Nombre ya existe',
                'email.email'=>'Correo Electronico no es valido',
                'password.min'=>'Contraseña debe contener minimo 6 caracteres',
                'password_confirmation.same'=>'Confirmar Contraseña debe concidir con la Contraseña',
                'type.not_in'=>'Tipo de Usuario debe seleccionar uno',
                'email.unique'=>'Correo Electronico ya existe',
                'rol_id.not_in'=>'Debe seleccionar un tipo de usuario',
                'empresa_id.not_in'=>'Debe seleccionar una empresa',
                );


        // Llamamos a Validator pasándole las reglas de validación.
        $validator=Validator::make($request->all(),$rules,$messages);

        // Si falla la validación redireccionamos de nuevo al formulario
        // enviando la variable Input (que contendrá todos los Input recibidos)
        // y la variable errors que contendrá los mensajes de error de validator.
        if ($validator->fails())
        {
            return Redirect::to('/usuarios/create')
            ->withInput()
            ->withErrors($validator->messages());
        }else{
            //dd($request->all());
            $user=new User();
            $user->name=$request->name;
            $user->email=$request->email;
            $user->password=bcrypt($request->password);
            $user->rol_id=$request->rol_id;
            $user->empresa_id=$request->empresa_id;
            $user->habilitado=true;
            $user->save();
            Session::flash('store','El usuario fue creado correctamente!!!');
            return Redirect::to('usuarios');

        }

        
    }
        
    public function show($id)
    {

    }
        
    public function edit($id)
    {
        $roles=Rol::all();
        $empresas=Empresa::all();
       return view("usuarios.edit",["user"=>User::findOrFail($id),"roles"=>$roles,"empresas"=>$empresas]);
    }
        
    public function update(Request $request,$id)
    {

        // Realizamos la validación de datos recibidos del formulario.
        $rules=array(
            'password'=>'required|min:6',
            'password_confirmation'=>'required|same:password',
            'rol_id'=>'required|not_in:0',
            'empresa_id'=>'required|not_in:0'
            );

        $messages= array (
                'password.required'=>'Contraseña es requerido',
                'password_confirmation.required'=>'Confirmar Contraseña es requerido',
                'rol_id.required'=>'Tipo de Usuario es requerido',
                'empresa_id.required'=>'La empresa es requerido',
                'password.min'=>'Contraseña debe contener minimo 10 caracteres',
                'password_confirmation.same'=>'Confirmar Contraseña debe concidir con la Contraseña',
                'empresa_id.not_in'=>'Debe seleccionar una empresa'
                );

        // Llamamos a Validator pasándole las reglas de validación.
        $validator=Validator::make($request->all(),$rules,$messages);

        // Si falla la validación redireccionamos de nuevo al formulario
        // enviando la variable Input (que contendrá todos los Input recibidos)
        // y la variable errors que contendrá los mensajes de error de validator.
        if ($validator->fails())
        {
            return Redirect::to('usuarios/'.$id.'/edit')
            ->withInput()
            ->withErrors($validator->messages());
        }else{
                $affectedRows = User::where('id','=',$id)
                ->update([
                    'password' =>bcrypt($request->get('password')),
                    'rol_id'=>$request->get('rol_id'),
                    'empresa_id'=>$request->get('empresa_id')]);
                Session::flash('update','El usuario se ha actualizado correctamente!!!');       
                return Redirect::to('usuarios');
            }
    }



    public function destroy($id)
    {
        $query=trim($id);
        $user = User::find($query);
        if($user->habilitado==true){
        $affectedRows = User::where('id','=',$id)
                ->update([
                    'habilitado'=>false]);
        Session::flash('destroy','El usuario a sido deshabilitado correctamente!!!');
        }else{
        $affectedRows = User::where('id','=',$id)
                ->update([
                    'habilitado'=>true]);
        Session::flash('destroy','El usuario a sido habilitado correctamente!!!');
        }
        
        return Redirect::to('usuarios');

    }

    public function edituser($id)
    {
       return view("usuarioa.edit",["user"=>User::findOrFail($id)]);
    }
        
    public function updateuser(Request $request,$id)
    {

        $affectedRows = User::where('id','=',$id)
        ->update(['name'=> $request->get('name'),
            'email'=>$request->get('email'),
            'password' =>bcrypt($request->get('password')),
            'type'=>$request->get('type')]);
        Session::flash('update','El usuario se ha actualizado correctamente!!!');       
        return Redirect::to('usuarios');

    }

    public function bitacora(Request $request){
        
        if ($request)
        {
            //$users=User::all();

            //Datos para la mostrar el mantenimiento
            $bitacoras=DB::table('bitacoras as b')->Select( DB::raw('u.id, u.name, u.email, u.habilitado, u.rol_id, u.ultimo_login, b.fecha, b.descripcion,p.id,p.numero,p."fechaInicio",p."fechaFinal"'))
            ->join('users as u','b.user_id','=','u.id')
            ->join('Planillas as p','b.planilla_id','=','p.id')
            ->get();
          // fin
            return view('bitacora.bitacora',["bitacoras"=>$bitacoras]);
        }
    }
}
