<?php

namespace bab115g9\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DireccionController extends Controller
{

    public function getRegiones(Request $request)
    {
        $regiones = DB::select('select * from getregiones(?)', array($request->get('id')));
        return $regiones;
    }

    public function getSubRegiones(Request $request)
    {
        $sub = DB::select('select * from getsubregiones(?)', array($request->get('id')));
        return $sub;
    }
}
