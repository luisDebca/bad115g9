<?php

namespace bab115g9\Http\Controllers;

use bab115g9\Empresa;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AdministracionEmpresaController extends Controller
{
    private $logo_path;
    private $defaultLogo;

    public function __construct()
    {
        $this->logo_path = public_path('/images');
        $this->defaultLogo = 'noimage.png';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = Empresa::all();
        return view('adminempresa.index', compact('empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminempresa.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateEmpresa($request);
        $request->validate(['nombre' => 'unique:Empresas']);
        $LogoName = $this->defaultLogo;
        if ($request->exists('logo')) {
            $logo = $request->file('logo');
            $this->createImageDirectoryIfNotExist();
            $LogoName = $this->getRandomName($logo->getClientOriginalExtension());

            Image::make($logo)->resize(250, null, function ($constraints) {$constraints->aspectRatio();
            })->save($this->logo_path . '/' . $LogoName);
            $logo->move($this->logo_path, $LogoName);
        }
        $empresa = new Empresa([
            'nombre' => $request->get('nombre'),
            'direccion' => $request->get('direccion'),
            'representateLegal' => $request->get('representanteLegal'),
            'nit' => $request->get('nit'),
            'nic' => $request->get('nic'),
            'telefono' => $request->get('telefono'),
            'email' => $request->get('email'),
            'web' => $request->get('web'),
            'logoDir'=>$LogoName,
        ]);

        $empresa->save();
        return redirect('/ad-empresa')->with('message','Datos de Empresa Guardados Exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Empresa::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = Empresa::find($id);
        return view('adminempresa.edit', compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editLogo = false;
        $empresa = Empresa::find($id);
        $LogoName = $empresa->logoDir;
        $this->validateEmpresa($request);
        if ($request->exists('logo')) {
            $editLogo = true;
            $logo = $request->file('logo');

            $this->createImageDirectoryIfNotExist();
            $LogoName = $this->getRandomName($logo->getClientOriginalExtension());

            Image::make($logo)->resize(250, null, function ($constraints) {$constraints->aspectRatio();})
                ->save($this->logo_path . '/' . $LogoName);
            $logo->move($this->logo_path, $LogoName);
        }
        $empresa->nombre = $request->get('nombre');
        $empresa->direccion = $request->get('direccion');
        $empresa->representateLegal = $request->get('representanteLegal');
        $empresa->nit = $request->get('nit');
        $empresa->nic = $request->get('nic');
        $empresa->telefono = $request->get('telefono');
        $empresa->email = $request->get('email');
        $empresa->web = $request->get('web');
        if($editLogo){
            $empresa->logoDir = $LogoName;
        }
        $empresa->save();
        return redirect('/ad-empresa')->with('message','Registros Actualizados Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = Empresa::find($id);
        $empresa->delete();

        return redirect('/ad-empresa')->with('message', 'Empresa removida correctamente.');
    }

    private function createImageDirectoryIfNotExist()
    {
        if (!is_dir($this->logo_path)) {
            mkdir($this->logo_path, 0777);
        }
    }

    private function getRandomName($fileType)
    {
        return sha1(date('YmdHis') . str_random(10)) . '.' .$fileType;
    }

    private function validateEmpresa(Request $request)
    {
        $request->validate([
            'nombre' => 'required|max:100',
            'direccion'=>'required|max:500',
            'nit'=>'required|max:17',
            'nic'=>'required|max:10',
            'telefono' => 'nullable|regex:/^[0-9]{8}$/|numeric',
            'email' => 'max:150',
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
    }
}
