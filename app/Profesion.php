<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
	protected $table = 'Profesiones';
    protected $fillable = ['nombre','descripcion'];

    public function empleados(){
        return $this->hasMany(Empleado::class);
    }
}
