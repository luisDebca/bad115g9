<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class TipoContrato extends Model
{
    protected $table = "TipoContratos";

    protected $fillable = [
        'tipo',
        'descripcion'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function contratos()
    {
        return $this->hasMany('App\ContratoTrabajo');
    }
}
