<?php

namespace bab115g9\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'bab115g9\Events\Event' => [
            'bab115g9\Listeners\EventListener',
        ],
        'Illuminate\Auth\Events\Login' => [
        'bab115g9\Listeners\SuccessfulLogin',
    ],
        
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
