<?php

namespace bab115g9;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    //
    protected $table = 'Paises';

    protected $fillable = ['nombre'];


     public function regiones() {
        return $this->hasMany('App\Region');
    }
}
