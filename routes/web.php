<?php

Route::middleware('auth')->group(function() {

    Route::get('/', function () {
        return view('welcome');
    });

    //----- Rutas Erick -----//
    Route::resource('empleado', 'EmpleadoController');
    Route::resource('genero', 'GeneroController');
    Route::resource('profesion', 'ProfesionController');
    Route::resource('estadocivil', 'EstadoCivilController');
    // prueba dinamico

    Route::get('/','WelcomeController@welcome');
    Route::resource('puestos','PuestosTrabajoController');
    Route::resource('descuentos','DescuentoController');
    Route::resource('ingresos','IngresoController');
    Route::resource('comisiones','ComisionController');
    Route::resource('rentas','RentaController');
    Route::get('puestos-puesto-nombres','PuestosResumenController@nombres');
    Route::get('puestos-puesto-rango-salario','PuestosResumenController@rangoSalario');
    Route::get('puestos-puesto-cuenta', 'PuestosResumenController@cuenta');
    Route::get('Empleado/chiefsEmployees/{id}','EmpleadoController@showChiefsEmployees');
    Route::get('SelectPais/{id}','EmpleadoController@getAllRegiones');
    Route::get('SelectRegion/{id}','EmpleadoController@getAllSubRegiones');

    Route::resource('planillas','PlanillaController');
    Route::resource('config','ConfiguracionController');
    Route::resource('ad-empresa', 'AdministracionEmpresaController');
    
    //Rutas de Milton
    Route::resource('empresa','EmpresaController');
    Route::resource('unidad-organizativa','UnidadOrganizativaController');    
    Route::resource('centro-costos','CentroCostosController');
    Route::post('unidad-organizativa/insert', 'UnidadOrganizativaController@insert')->name('unidad-organizativa.insert');
    Route::post('unidad-organizativa/save', 'UnidadOrganizativaController@save')->name('unidad-organizativa.save');
    Route::get('unidad-organizativa/{id}/subunidades', 'UnidadOrganizativaController@subunidades')->name('unidad-organizativa.subunidades');
    Route::delete('centro-costos/remove', 'CentroCostosController@remove')->name('centro-costos.remove');

    // Lineas nuevas del la rama de Luis >>>>>>> luis
    Route::get('get-departamentos-planilla', 'PlanillaHelperController@getDepartamentos');
    Route::get('get-sub-unidades-planilla', 'PlanillaHelperController@getSubUnidades');
    Route::get('get-empleados-planilla', 'PlanillaHelperController@getUnidadEmpleados');

    Route::get('get-planilla-ingresos', 'PlanillaHelperController@getPlanillaIngresos');
    Route::get('get-planilla-descuentos', 'PlanillaHelperController@getPlanillaDescuentos');
    Route::get('get-planilla-comisiones', 'PlanillaHelperController@getPlanillaComisiones');

    Route::post('planilla-seleccionar-unidades','PlanillaProcessController@unidades');
    Route::post('planilla-seleccionar-empleados','PlanillaProcessController@nuevaPlanilla');

    Route::get('planilla-modificar-empleados/{numero}','PlanillaProcessController@mostrarEmpleados');

    Route::get('planilla-modificar-empleados/{numero}','PlanillaProcessController@mostrarEmpleados');

    Route::get('planilla-reporte-empleados/{numero}','PlanillaProcessController@planillaPdf');

    Route::get('planilla-nuevo-ingreso/{planilla}','PlanillaProcessController@agregarIngresoPlanilla');
    Route::post('planilla-nuevo-ingreso/{planilla}','PlanillaProcessController@guardarIngresoPlanilla');

    Route::get('planilla-nuevo-descuento/{planilla}','PlanillaProcessController@agregarDescuentoPlanilla');
    Route::post('planilla-nuevo-descuento/{planilla}','PlanillaProcessController@guardarDescuentoPlanilla');

    Route::get('planilla-nuevo-comision/{planilla}','PlanillaProcessController@agregarComisionPlanilla');
    Route::post('planilla-nuevo-comision/{planilla}','PlanillaProcessController@guardarComisionPlanilla');

    Route::get('planilla-editar/{planilla}','PlanillaProcessController@editarPlanillas');
    Route::post('planilla-actualizar/{planilla}','PlanillaProcessController@actualizarPlanillas');

    Route::get('planilla-centros-costos','PlanillaProcessController@getCentrosAfectados');

    Route::get('eliminar-planilla-{planilla}-ingreso-{ingreso}','PlanillaProcessController@eliminarPlanillaIngreso');
    Route::get('eliminar-planilla-{planilla}-descuento-{descuento}','PlanillaProcessController@eliminarPlanillaDescuento');
    Route::get('eliminar-planilla-{planilla}-comision-{comision}','PlanillaProcessController@eliminarPlanillaComision');
});

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');




Route::get('/info', 'HomeController@info');
Route::group(['middleware' =>  ['auth','admin']], function () {
      Route::resource('usuarios', 'HomeController'); 
      Route::get('usuarios/{id}','HomeController@edituser');
      Route::get('usuarios/{id}','HomeController@updateuser');
      Route::get('bitacora','HomeController@bitacora');
});

Route::get('generate-pdf','PlanillaReportController@testReport');

Route::get('get-regiones','DireccionController@getRegiones');
