create or replace function getDepartamentos(in e int) returns table (id int,nombre varchar)
as $$
begin
  return query
    select uo.id, uo.nombre from "UnidadesOrganizacionales" as uo
    where uo.empresa_id = e and uo."unidadPadre_id" isnull;
end;
$$ language 'plpgsql';

create or replace function getCountSubUnidad(in padre int) returns int
as $$
begin
  return (select count(*) from "UnidadesOrganizacionales" as uo
          where uo."unidadPadre_id" = padre);
end;
$$ language 'plpgsql';

create or replace function getDepartamentosResumen(in e int) returns table (id int,nombre varchar,sub bigint)
as $$
begin
  return query
    select uo.id, uo.nombre,
           (select count(*) from "UnidadesOrganizacionales" as sub where sub.id = uo.id)
    from "UnidadesOrganizacionales" as uo
    where uo.empresa_id = e and uo."unidadPadre_id" isnull;
end;
$$ language 'plpgsql';

create or replace function getPaises() returns table (id int, nombre varchar)
as $$
begin
  return query
    select p.id,p.nombre from "Paises" as p;
end; $$
  language 'plpgsql';

create or replace function getRegiones(in p int) returns table (id int, nombre varchar)
as $$
begin
  return query
    select r.id, r.nombre from "Regiones" as r
    where r.pais_id = p;
end; $$
  language 'plpgsql';

create or replace function getSubRegiones(in r int) returns table (id int, nombre varchar)
as $$
begin
  return query
    select sub.id, sub.nombre from "Subregiones" as sub
    where sub.region_id = r;
end; $$
  language 'plpgsql';

create or replace function getEstados(in sub int) returns table (id int, nombre varchar)
as $$
begin
  return query
    select e.id, e.detalle from "Estados" as e
    where e.subregion_id = sub;
end; $$
  language 'plpgsql';

create or replace function getSubunidades(in e int, in padre int) returns table (id int, nombre varchar, tipo varchar, sub bigint)
as $$
begin
  return query
    select uo.id, uo.nombre, uo.tipo, (select count(*) from "UnidadesOrganizacionales" as subUO where subUO."unidadPadre_id" = uo.id)
    from "UnidadesOrganizacionales" as uo
    where uo.empresa_id = e
      and uo."unidadPadre_id" = padre;
end; $$
  language 'plpgsql';

create or replace function getEmpleados(in uo int) returns table (id int, nombre text)
as $$
begin
  return query
    select emp.id, concat(emp."primerNombre",' ',emp."segundoNombre",' ', emp."apellidoPaterno",' ', emp."apellidoMaterno")  from "Empleados" as emp
                                                                                                                                    inner join "ContratosTrabajo" CT on emp.id = CT.empleado_id
    where CT.unidad_id = uo;
end; $$
  language 'plpgsql';

create or replace function getMaxNumeroPlanilla(in e int) returns varchar
as $$
declare
  sqlresult varchar;
begin
  sqlresult := (select max(planilla.numero) from "Planillas" as planilla where planilla.empresa = e);
  if sqlresult isnull then
    sqlresult = '0';
  end if;
  return sqlresult;
end; $$
  language 'plpgsql';

create or replace function ingresarPlanilla(in nuevo varchar, in contrato int) returns void
as $$
begin
  insert into "Planillas" (numero, "fechaInicio", "fechaFinal", created_at, updated_at, contrato_id)
  values (nuevo,current_date,null,current_timestamp,current_timestamp,contrato);
end; $$
  language 'plpgsql';

create or replace function getContratoActual(in emp int) returns int
as $$
begin
  return (select ct.id
          from "ContratosTrabajo" as ct
          where ct.empleado_id = emp
            and ct.habilitado = true
            and ct."fechaFin" isnull );
end; $$
  language 'plpgsql';

create or replace function getPlanillas(in emp int)
  returns table (numero varchar, inicio date, fin date,empleados bigint, validado boolean)
as $$
begin
  return query
    select planilla.numero, max(planilla."fechaInicio"), max(planilla."fechaFinal"), count(*), bool_and(planilla.validado) from "Planillas" as planilla
    where planilla.empresa = emp
    group by planilla.numero
    order by planilla.numero desc;
end;$$
  language 'plpgsql';

create or replace function getTotalSalarioBruto(in num varchar) returns double precision
as $$
begin
  return (select sum(CT.salario) from "Planillas" as planilla
                                        join "ContratosTrabajo" CT on planilla.contrato_id = CT.id
          where planilla.numero = num);
end; $$
  language 'plpgsql';

create or replace function getTotalIngresos(in num varchar) returns double precision
as $$
begin
  return (select sum(IP.monto) from "Planillas" as planilla
                                      join "IngresosPlanilla" IP on planilla.id = IP.planilla_id
          where planilla.numero = num);
end; $$
  language 'plpgsql';

create or replace function getTotalDescuentos(in num varchar) returns double precision
as $$
begin
  return (select sum(DP.monto) from "Planillas" as planilla
                                      join "DescuentosPlanilla" DP on planilla.id = DP.planilla_id
          where planilla.numero = num);
end; $$
  language 'plpgsql';

create or replace function getTotalComision(in num varchar) returns double precision
as $$
begin
  return (select sum(V.monto) from "Planillas" as planilla
                                     join "Ventas" V on planilla.id = V.planilla_id
          where planilla.numero = num);
end; $$
  language 'plpgsql';

create or replace function getSumatoriaIngresos(in pla int) returns double precision
as $$
begin
  return (select sum(IP.monto) from "Planillas" as planilla
                                      join "IngresosPlanilla" IP on planilla.id = IP.planilla_id
          where planilla.id = pla);
end; $$
  language 'plpgsql';

create or replace function getSumatoriaDescuentos(in pla int) returns double precision
as $$
begin
  return (select sum(DP.monto) from "Planillas" as planilla
                                      join "DescuentosPlanilla" DP on planilla.id = DP.planilla_id
          where planilla.id = pla);
end; $$
  language 'plpgsql';

create or replace function getSumatoriaComision(in pla int) returns double precision
as $$
begin
  return (select sum(V.monto) from "Planillas" as planilla
                                     join "Ventas" V on planilla.id = V.planilla_id
          where planilla.id = pla);
end; $$
  language 'plpgsql';

create or replace function getEmpleadosPlanilla(in pla varchar)
  returns table (id int, planilla int, nombre text, unidad varchar, puesto varchar,salario double precision)
as $$
begin
  return query
    select
      E.id,
      planilla.id,
      concat(E."primerNombre",' ',E."segundoNombre",' ', E."apellidoPaterno",' ', E."apellidoMaterno"),
      uo.nombre,
      PT.nombre,
      CT.salario
    from "Planillas" as planilla
           join "ContratosTrabajo" CT on planilla.contrato_id = CT.id
           join "Empleados" E on CT.empleado_id = E.id
           join "UnidadesOrganizacionales" UO on CT.unidad_id = UO.id
           join "PuestosTrabajo" PT on CT.puesto_id = PT.id
    where planilla.numero = pla;
end; $$
  language 'plpgsql';

create or replace function getPlanillaIngresos(in pla int)
  returns table (planilla int, ingreso int, nombre varchar, monto double precision, estado boolean)
as $$
begin
  return query
    select IP.planilla_id, IP.ingreso_id, I.nombre, IP.monto, IP.estado from "Planillas" as planilla
                                                                               join "IngresosPlanilla" IP on planilla.id = IP.planilla_id
                                                                               join "Ingresos" I on IP.ingreso_id = I.id
    where planilla.id = pla;
end; $$
  language 'plpgsql';

create or replace function getPlanillaDescuentos(in pla int)
  returns table (planilla int, ingreso int, nombre varchar, monto double precision, estado boolean)
as $$
begin
  return query
    select DP.planilla_id, DP.descuento_id, D.nombre, DP.monto, DP.estado from "Planillas" as planilla
                                                                                 join "DescuentosPlanilla" DP on planilla.id = DP.planilla_id
                                                                                 join "Descuentos" D on DP.descuento_id = D.id
    where planilla.id = pla;
end; $$
  language 'plpgsql';

create or replace function getPlanillaComisiones(in pla int)
  returns table (planilla int, ingreso int, nombre varchar, monto double precision, estado boolean)
as $$
begin
  return query
    select V.planilla_id, V.comision_id, C.nombre, V.monto, V.estado from "Planillas" as planilla
                                                                            join "Ventas" V on planilla.id = V.planilla_id
                                                                            join "Comisiones" C on V.comision_id = C.id
    where planilla.id = pla;
end; $$
  language 'plpgsql';

create or replace function crearDescuentoRenta() returns trigger
as $BODY$
begin
  insert into "Descuentos"(nombre, taza, created_at, updated_at, renta_id, "montoFijo", tipo, empresa)
  values(concat('Renta Tramo ',NEW.tramo),0.00,current_timestamp,current_timestamp,NEW.id,0.00,'RENTA',NEW.empresa);
  return NEW;
end; $BODY$
  language 'plpgsql';

create trigger trigger_crearDescuentoRenta
  AFTER INSERT
  ON "Rentas"
  for each row
execute procedure crearDescuentoRenta();

create or replace function getSalario(in pla int) returns double precision
as $$
begin
  return (select CT.salario from "Planillas" as planilla
                                   join "ContratosTrabajo" CT on planilla.contrato_id = CT.id
          where planilla.id = pla);
end; $$
  language 'plpgsql';

create or replace function crearIngresoPlanilla(in planilla int, in ingreso int, in valor double precision, in inicio date, in fin date, in activo boolean) returns void
as $$
begin
  insert into "IngresosPlanilla" (planilla_id, ingreso_id, monto, desde, hasta, estado)
  values (planilla, ingreso, valor, inicio, fin, activo);
end; $$
  language 'plpgsql';

create or replace function crearDescuentoPlanilla(in planilla int, in descuento int, in valor double precision, in inicio date, in fin date, in activo boolean) returns void
as $$
begin
  insert into "DescuentosPlanilla" (planilla_id, descuento_id, monto, desde, hasta, estado)
  values (planilla, descuento, valor, inicio, fin, activo);
end; $$
  language 'plpgsql';

create or replace function crearComisionPlanilla(in planilla int, in comision int, in valor double precision, in inicio date, in fin date, in activo boolean) returns void
as $$
begin
  insert into "Ventas" (planilla_id, comision_id, monto, desde, hasta, estado)
  values (planilla, comision, valor, inicio, fin, activo);
end; $$
  language 'plpgsql';

create or replace function getPlanillaCentroCosto(in pla varchar)
  returns table (id int, nombre varchar, monto double precision)
as $$
begin
  return query
    select UO.id, UO.nombre, sum(CC.monto)/count(UO.id) from "Planillas" as planilla
                                                               join "ContratosTrabajo" CT on planilla.contrato_id = CT.id
                                                               join "UnidadesOrganizacionales" UO on CT.unidad_id = UO.id
                                                               join "CentroCostos" CC on UO.id = CC.unidad_id
    where planilla.numero = pla
      and CC.anio = DATE_PART('year', CURRENT_DATE)
    group by UO.id
    order by UO.nombre;
end;$$
  language 'plpgsql';

create or replace function eliminarIngresoPlanilla(in pla int, in ing int) returns void
as $$
begin
  delete from "IngresosPlanilla" as IP
  where IP.planilla_id = pla
    and  IP.ingreso_id = ing;
end; $$
  language 'plpgsql';

create or replace function eliminarDescuentoPlanilla(in pla int, in des int) returns void
as $$
begin
  delete from "DescuentosPlanilla" as DP
  where DP.planilla_id = pla
    and DP.descuento_id = des;
end; $$
  language 'plpgsql';

create or replace function eliminarComisionPlanilla(in pla int, in com int) returns void
as $$
begin
  delete from "Ventas" as ventas
  where ventas.planilla_id = pla
    and ventas.comision_id = com;
end; $$
  language 'plpgsql';


CREATE OR REPLACE FUNCTION public.showchiefs(ident INT)
    RETURNS TABLE(id integer, codigo character varying, primerNombre character varying, segundoNombre character varying, apellidoPaterno character varying, apellidoMaterno character varying, apellidoCasada character varying, fechaNacimiento date, dui character varying, nit character varying, isss character varying, nup character varying, pasaporte character varying, emailPersonal character varying, emailInstitucional character varying, created_at timestamp without time zone, updated_at timestamp without time zone, genero_id integer, estadoCivil_id integer, profesion_id integer, jefe_id integer, estado_id integer, empresa integer)
AS $$

	BEGIN
		RETURN QUERY SELECT
			"Empleados".id,
			"Empleados".codigo,
			"Empleados"."primerNombre",
			"Empleados"."segundoNombre",
			"Empleados"."apellidoPaterno",
			"Empleados"."apellidoMaterno",
			"Empleados"."apellidoCasada",
			"Empleados"."fechaNacimiento",
			"Empleados".dui,
			"Empleados".nit,
			"Empleados".isss,
			"Empleados".nup,
			"Empleados".pasaporte,
			"Empleados"."emailPersonal",
			"Empleados"."emailInstitucional",
			"Empleados".created_at,
			"Empleados".updated_at,
			"Empleados".genero_id,
			"Empleados"."estadoCivil_id",
			"Empleados".profesion_id,
			"Empleados".jefe_id,
			"Empleados".estado_id,
			"Empleados".empresa
		FROM
			"Empleados"
		WHERE
			"Empleados".id = ident;
	END; $$
LANGUAGE 'plpgsql';

create or replace function getResumenEmpleados(in emp int)
  returns table (id integer, codigo varchar, nombre text, genero varchar, estado varchar, profesion varchar, direccion text)
as $$
begin
  return query
    select
      empleado.id,
      empleado.codigo,
      concat(empleado."primerNombre",' ',empleado."segundoNombre",' ',empleado."apellidoPaterno",' ',empleado."apellidoMaterno"),
      G.tipo,
      EC.tipo,
      P.nombre,
      concat(E.detalle,', ',S.nombre,', ',R.nombre,', ',P2.nombre)
    from "Empleados" as empleado
           join "EstadoCiviles" EC on empleado."estadoCivil_id" = EC.id
           join "Profesiones" P on empleado.profesion_id = P.id
           join "Generos" G on empleado.genero_id = G.id
           left join "Estados" E on empleado.id = E.empleado_id
           left join "Subregiones" S on E.subregion_id = S.id
           left join "Regiones" R on S.region_id = R.id
           left join "Paises" P2 on R.pais_id = P2.id
    where empleado.empresa = emp;
end;$$
  language 'plpgsql';